//
//  customCircularImage.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 5/29/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class customCircularImage: UIImageView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderColor = UIColor.whiteColor().CGColor
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }

}
