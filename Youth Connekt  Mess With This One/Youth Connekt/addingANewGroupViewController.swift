//
//  addingANewGroupViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 6/5/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class addingANewGroupViewController: UIViewController,UITextFieldDelegate {
    
    // -------------- user data-----------------
    var user_name:String?
    var user_first_name:String?
    var user_last_name:String?
    var user_gender:String?
    var user_status:String?
    var user_image:UIImage?
    var user_phone_number:String?
    var user_email:String?
    var user_password:String?
    var user_group:String?
    var user_subgroup:String?
    var user_affiliation:String?
    var user_province:String?
    var user_district:String?
    var user_sector:String?
    // -------------- user data-----------------
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    
    @IBOutlet weak var mainNavBar: UINavigationBar!
    var containerView:UIView = UIView()
    
    // Screen Properties
    var spacingViews:CGFloat = 4
    let sideSpace:CGFloat = 40
    
    // Outlets
    var titleLabel:UILabel = UILabel()
    var firstNameTextField:UITextField = UITextField()
    var lastNameTextField:UITextField = UITextField()
    var emailTextField:UITextField = UITextField()
    var phoneTextField:UITextField = UITextField()
    var addMyGroupButton:UIButton = UIButton()
    
    // type of iphone
    var iphoneType:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        createViews(iphoneType!, containerViewHeight: (self.view.frame.height - (mainNavBar.frame.origin.y + mainNavBar.frame.height)),containerViewWidth: self.view.frame.width)
        createPicker()
    }
    
    func createViews(typeOfIphone:Int, containerViewHeight:CGFloat, containerViewWidth:CGFloat)
    {
        var youthConnektLogoSize:CGFloat = 0
        var spaceLogo_NavBar:CGFloat = 0
        var middlePartOffset:CGFloat = 0
        
        switch typeOfIphone{
        case 4:
            spacingViews = 20
            break
        case 5:
            spacingViews = 30
            break
        case 6:
            spacingViews = 50
            break
        case 61:
            spacingViews = 60
            
            break
        default:
            spacingViews = 10
        }
        
        containerView.frame = CGRect(x: 0, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height), width: containerViewWidth, height: containerViewHeight)
        containerView.backgroundColor = UIColor(red: (232/255), green: (227/255), blue: (216/255), alpha: 1)
        self.view.addSubview(containerView)
        
        //============================ titleLabel ===================================
        titleLabel.frame = CGRect(x: ((containerView.frame.width / 2) - 75), y: spacingViews, width: 150, height: 30)
        titleLabel.tag = 1
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.font = UIFont.systemFontOfSize(20, weight: 0.1)
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.text = "Group Leader"
        
        containerView.addSubview(titleLabel)

        
        //============================ firstNameTextField ===================================
        firstNameTextField.frame = CGRect(x: sideSpace, y: (titleLabel.frame.origin.y + titleLabel.frame.height + (spacingViews*2)), width: containerView.frame.width - (sideSpace*2), height: 30)
        firstNameTextField.delegate = self
        firstNameTextField.delegate = self
        firstNameTextField.tag = 2
        firstNameTextField.placeholder = " first name"
        firstNameTextField.textColor = UIColor.blueColor()
        
        var firstNameBorder = CALayer()
        var width = CGFloat(0.75)
        firstNameBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        firstNameBorder.borderWidth = width
        firstNameBorder.frame = CGRectMake(0, firstNameTextField.frame.size.height - width, firstNameTextField.frame.size.width, firstNameTextField.frame.size.height)
        firstNameTextField.layer.addSublayer(firstNameBorder)
        firstNameTextField.layer.masksToBounds = true
        
        containerView.addSubview(firstNameTextField)
        //============================ firstNameTextField ===================================
        
        lastNameTextField.frame = CGRect(x: sideSpace, y: (firstNameTextField.frame.origin.y + firstNameTextField.frame.height + spacingViews), width: containerView.frame.width - (sideSpace*2), height: 30)
        lastNameTextField.delegate = self
        lastNameTextField.delegate = self
        lastNameTextField.tag = 2
        lastNameTextField.placeholder = " last name"
        lastNameTextField.textColor = UIColor.blueColor()
        
        var lastNameBorder = CALayer()
        lastNameBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        lastNameBorder.borderWidth = width
        lastNameBorder.frame = CGRectMake(0, lastNameTextField.frame.size.height - width, lastNameTextField.frame.size.width, lastNameTextField.frame.size.height)
        lastNameTextField.layer.addSublayer(lastNameBorder)
        lastNameTextField.layer.masksToBounds = true
        
        containerView.addSubview(lastNameTextField)

        
        //============================ emailTextField ===================================
        emailTextField.frame = CGRect(x: sideSpace, y: (lastNameTextField.frame.origin.y + lastNameTextField.frame.height + spacingViews), width: containerView.frame.width - (sideSpace*2), height: 30)
        emailTextField.delegate = self
        
        emailTextField.tag = 3
        emailTextField.text = self.user_email!
        emailTextField.textColor = UIColor.blueColor()
        
        var emailBorder = CALayer()
        emailBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        emailBorder.borderWidth = width
        emailBorder.frame = CGRectMake(0, emailTextField.frame.size.height - width, emailTextField.frame.size.width, emailTextField.frame.size.height)
        emailTextField.layer.addSublayer(emailBorder)
        emailTextField.layer.masksToBounds = true
        
        containerView.addSubview(emailTextField)
        //============================ emailTextField ===================================
        
        //============================ phoneTextField ===================================
        phoneTextField.frame = CGRect(x: sideSpace, y: (emailTextField.frame.origin.y + emailTextField.frame.height + spacingViews) , width: containerView.frame.width - (sideSpace*2), height: 30)
        phoneTextField.delegate = self
        
        phoneTextField.tag = 4
        phoneTextField.placeholder = "phone"
        phoneTextField.textColor = UIColor.blueColor()
        
        var phoneBorder = CALayer()
        phoneBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        phoneBorder.borderWidth = width
        phoneBorder.frame = CGRectMake(0, phoneTextField.frame.size.height - width, phoneTextField.frame.size.width, phoneTextField.frame.size.height)
        phoneTextField.layer.addSublayer(phoneBorder)
        phoneTextField.layer.masksToBounds = true
        
        containerView.addSubview(phoneTextField)
        //============================ phoneTextField ===================================
        
        addMyGroupButton.frame = CGRect(x: (containerView.frame.width/2 - 80), y: (containerView.frame.height - 100), width: 160, height: 30)
        addMyGroupButton.tag = 5
        addMyGroupButton.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        addMyGroupButton.layer.borderWidth = 1
        addMyGroupButton.layer.cornerRadius = 3
        addMyGroupButton.setTitleColor(UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1), forState: .Normal)
        addMyGroupButton.setTitle("Add my Group", forState: .Normal)
        addMyGroupButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        addMyGroupButton.titleLabel?.textAlignment = NSTextAlignment.Center
        addMyGroupButton.addTarget(self, action: "addMyGroupButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(addMyGroupButton)
    }
    
    // button click handlers
    func addMyGroupButtonClick(sender:UIButton){
        if firstNameTextField.text.isEmpty || lastNameTextField.text.isEmpty || emailTextField.text.isEmpty || phoneTextField.text.isEmpty{
            showAlertController("Please enter the required information!")
        }
        else if !emailTextField.text.isEmail || phoneTextField.text.isEmpty{
            showAlertController("Please enter your correct email and phone number!")
        }
//        else if !phoneTextField.text.isPhoneNumber{
//            showAlertController("Please enter your correct phone number!")
//        }
        else {
            // ============ Segue to the next page ================
            
            self.user_name = ""
            self.user_first_name = firstNameTextField.text
            self.user_last_name = lastNameTextField.text
            self.user_email = emailTextField.text
            self.user_phone_number = phoneTextField.text
            
            performSegueWithIdentifier("showLeaderIstAddGroupPage", sender: self)
        }
    }
    
    func showAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == emailTextField{
            if !emailTextField.text.isEmail{
                emailTextField.textColor = UIColor.redColor()
            }
            else{
                emailTextField.textColor = UIColor.blueColor()
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    var kbHeight: CGFloat!
    var textFieldTag: Int = -1
    
    func animateTextField(up: Bool) {
        var movement = (up ? -kbHeight : kbHeight)
        
        UIView.animateWithDuration(0.3, animations: {
            self.containerView.frame = CGRectOffset(self.containerView.frame, 0, movement)
        })
    }
    
    override func viewWillAppear(animated:Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                let textfield:UITextField = self.view.viewWithTag(textFieldTag) as! UITextField
                if((keyboardSize.height + 25) > (self.containerView.frame.height - textfield.frame.origin.y)){
                    kbHeight = (keyboardSize.height - (self.containerView.frame.height - textfield.frame.origin.y)) + 35
                    
                }
                else{
                    kbHeight = 0
                }
                self.animateTextField(true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.animateTextField(false)
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "About"],
            ["title" : "Settings"],
            ["title" : "Share"],
            ["title" : "Sign In"]
        ]
    }
    
    @IBAction func pickerSelect(sender: UIBarButtonItem)
    {
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createPicker()
    {
        pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func openPicker()
    {
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    func buttonClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/360), green: (47/360), blue: (61/360), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        closePicker()
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    
    // =================================================================
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        textFieldTag = textField.tag
        return true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showLeaderIstAddGroupPage"  {
            
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! addGroupIstPageViewController
            toViewController.iphoneType = self.iphoneType!
            toViewController.transitioningDelegate = self.transitionManager
            
            // transfer user data
            toViewController.user_email = self.user_email!
            toViewController.user_password = self.user_password!
            toViewController.user_gender = self.user_gender!
            toViewController.user_status = self.user_status!
            toViewController.user_name = self.user_name!
            toViewController.user_first_name = self.user_first_name!
            toViewController.user_last_name = self.user_last_name!
            toViewController.user_phone_number = self.user_phone_number!
            
        }
    }
    
    // unwind the segue
    @IBAction func handleBackButtonClick(sender:UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
