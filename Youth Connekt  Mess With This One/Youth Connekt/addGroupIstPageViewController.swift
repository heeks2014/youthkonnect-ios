//
//  addGroupIstPageViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 6/5/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class addGroupIstPageViewController: UIViewController,UITextFieldDelegate {
    
    // -------------- user data-----------------
    var user_name:String?
    var user_first_name:String?
    var user_last_name:String?
    var user_gender:String?
    var user_status:String?
    var user_image:UIImage?
    var user_phone_number:String?
    var user_email:String?
    var user_password:String?
    var user_group:String?
    var user_subgroup:String?
    var user_affiliation:String?
    var user_province:String?
    var user_district:String?
    var user_sector:String?
    // -------------- user data-----------------
    
    // type of iphone
    var iphoneType:Int?
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    
    @IBOutlet weak var mainNavBar: UINavigationBar!
    var containerView:UIView = UIView()
    
    // Screen Properties
    var spacingViews:CGFloat = 4
    let sideSpace:CGFloat = 40
    
    // Outlets
    var titleLabel:UILabel = UILabel()
    
    var groupTypeTextField:UITextField = UITextField()
    var groupTypeDropDownButton:UIButton = UIButton()
    
    var subGroupTextField:UITextField = UITextField()
    var subGroupDropDownButton:UIButton = UIButton()
    
    var groupNameTextField:UITextField = UITextField()
    var groupNameDropDownButton:UIButton = UIButton(frame: CGRectZero)
    
    var addMyGroupButton:UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        createViews(iphoneType!, containerViewHeight: (self.view.frame.height - (mainNavBar.frame.origin.y + mainNavBar.frame.height)),containerViewWidth: self.view.frame.width)
        createPicker()
        //        createTheSchoolTextField()
    }
    
    func createViews(typeOfIphone:Int, containerViewHeight:CGFloat, containerViewWidth:CGFloat)
    {
        var youthConnektLogoSize:CGFloat = 0
        var spaceLogo_NavBar:CGFloat = 0
        var middlePartOffset:CGFloat = 0
        
        switch typeOfIphone{
        case 4:
            spacingViews = 20
            break
        case 5:
            spacingViews = 40
            break
        case 6:
            spacingViews = 50
            break
        case 61:
            spacingViews = 60
            
            break
        default:
            spacingViews = 10
        }
        
        containerView.frame = CGRect(x: 0, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height), width: containerViewWidth, height: containerViewHeight)
        containerView.backgroundColor = UIColor(red: (232/255), green: (227/255), blue: (216/255), alpha: 1)
        self.view.addSubview(containerView)
        
        //============================ titleLabel ===================================
        titleLabel.frame = CGRect(x: ((containerView.frame.width / 2) - 75), y: spacingViews, width: 150, height: 30)
        titleLabel.tag = 1
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.font = UIFont.systemFontOfSize(20, weight: 0.1)
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.text = "Group Leader"
        
        containerView.addSubview(titleLabel)
        
        
        //============================ nameTextField ===================================
        groupTypeTextField.frame = CGRect(x: sideSpace, y: (titleLabel.frame.origin.y + titleLabel.frame.height + (spacingViews*2)), width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        groupTypeTextField.delegate = self
        groupTypeTextField.userInteractionEnabled = false
        groupTypeTextField.tag = 2
        groupTypeTextField.placeholder = "group type"
        groupTypeTextField.textColor = UIColor.blueColor()
        
        var groupTypeBorder = CALayer()
        var width = CGFloat(0.75)
        groupTypeBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        groupTypeBorder.borderWidth = width
        groupTypeBorder.frame = CGRectMake(0, groupTypeTextField.frame.size.height - width, groupTypeTextField.frame.size.width, groupTypeTextField.frame.size.height)
        groupTypeTextField.layer.addSublayer(groupTypeBorder)
        groupTypeTextField.layer.masksToBounds = true
        
        containerView.addSubview(groupTypeTextField)
        //============================ nameTextField ===================================
        //============================ groupTypeDropDownButton ==========================
        
        groupTypeDropDownButton.frame = CGRect(x: (groupTypeTextField.frame.origin.x + groupTypeTextField.frame.width), y: (groupTypeTextField.frame.origin.y), width: 22, height: 30)
        groupTypeDropDownButton.tag = 3
        groupTypeDropDownButton.layer.borderColor = UIColor.whiteColor().CGColor
        groupTypeDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        groupTypeDropDownButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        groupTypeDropDownButton.titleLabel?.textAlignment = NSTextAlignment.Center
        groupTypeDropDownButton.addTarget(self, action: "groupTypesDropDownSelect:", forControlEvents: UIControlEvents.TouchUpInside)
        
        var groupTypeDropDownBorder = CALayer()
        groupTypeDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        groupTypeDropDownBorder.borderWidth = width
        groupTypeDropDownBorder.frame = CGRectMake(0, groupTypeDropDownButton.frame.size.height - width, groupTypeDropDownButton.frame.size.width, groupTypeDropDownButton.frame.size.height)
        groupTypeDropDownButton.layer.addSublayer(groupTypeDropDownBorder)
        groupTypeDropDownButton.layer.masksToBounds = true
        
        containerView.addSubview(groupTypeDropDownButton)
        
        //============================ groupTypeDropDownButton ============================
        
        //============================ emailTextField ===================================
        subGroupTextField.frame = CGRect(x: sideSpace, y: (groupTypeTextField.frame.origin.y + groupTypeTextField.frame.height + spacingViews), width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        subGroupTextField.delegate = self
        subGroupTextField.userInteractionEnabled = false
        subGroupTextField.tag = 4
        subGroupTextField.placeholder = "sub group"
        subGroupTextField.textColor = UIColor.blueColor()
        
        var subGroupBorder = CALayer()
        subGroupBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        subGroupBorder.borderWidth = width
        subGroupBorder.frame = CGRectMake(0, subGroupTextField.frame.size.height - width, subGroupTextField.frame.size.width, subGroupTextField.frame.size.height)
        subGroupTextField.layer.addSublayer(subGroupBorder)
        subGroupTextField.layer.masksToBounds = true
        
        subGroupTextField.alpha = 0
        subGroupTextField.hidden = true
        
        containerView.addSubview(subGroupTextField)
        
        
        subGroupDropDownButton.frame = CGRect(x: (subGroupTextField.frame.origin.x + subGroupTextField.frame.width), y: (subGroupTextField.frame.origin.y), width: 22, height: 30)
        subGroupDropDownButton.tag = 5
        subGroupDropDownButton.layer.borderColor = UIColor.whiteColor().CGColor
        subGroupDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        subGroupDropDownButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        subGroupDropDownButton.titleLabel?.textAlignment = NSTextAlignment.Center
        subGroupDropDownButton.addTarget(self, action: "groupTypesDropDownSelect:", forControlEvents: UIControlEvents.TouchUpInside)
        containerView.addSubview(subGroupDropDownButton)
        
        var subGroupDropDownBorder = CALayer()
        subGroupDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        subGroupDropDownBorder.borderWidth = width
        subGroupDropDownBorder.frame = CGRectMake(0, subGroupDropDownButton.frame.size.height - width, subGroupDropDownButton.frame.size.width, subGroupDropDownButton.frame.size.height)
        subGroupDropDownButton.layer.addSublayer(subGroupDropDownBorder)
        subGroupDropDownButton.layer.masksToBounds = true
        
        subGroupDropDownButton.alpha = 0
        subGroupDropDownButton.hidden = true
        
        containerView.addSubview(subGroupDropDownButton)
        //============================ emailTextField ===================================
        
        //============================ schoolTextField ===================================
        groupNameTextField.frame = CGRect(x: self.sideSpace, y: (self.subGroupTextField.frame.origin.y + self.subGroupTextField.frame.height + self.spacingViews), width: self.containerView.frame.width - (sideSpace*2), height: 30)
        groupNameTextField.delegate = self
        
        groupNameTextField.tag = 6
        groupNameTextField.placeholder = "group name"
        groupNameTextField.textColor = UIColor.blueColor()
        
        var groupNameBorder = CALayer()
        groupNameBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        groupNameBorder.borderWidth = width
        groupNameBorder.frame = CGRectMake(0, groupNameTextField.frame.size.height - width, groupNameTextField.frame.size.width, groupNameTextField.frame.size.height)
        groupNameTextField.layer.addSublayer(groupNameBorder)
        groupNameTextField.layer.masksToBounds = true
        
        groupNameTextField.alpha = 0
        groupNameTextField.hidden = true
        
        containerView.addSubview(groupNameTextField)
        //============================ schoolTextField ===================================
        
        addMyGroupButton.frame = CGRect(x: (containerView.frame.width/2 - 55), y: (containerView.frame.height - 100), width: 110, height: 30)
        addMyGroupButton.tag = 7
        addMyGroupButton.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        addMyGroupButton.layer.borderWidth = 1
        addMyGroupButton.layer.cornerRadius = 3
        addMyGroupButton.setTitleColor(UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1), forState: .Normal)
        addMyGroupButton.setTitle("Continue", forState: .Normal)
        addMyGroupButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        addMyGroupButton.titleLabel?.textAlignment = NSTextAlignment.Center
        addMyGroupButton.addTarget(self, action: "continueButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(addMyGroupButton)
    }
    
    //------------------------------------------------------------------------------------------
    // School Name Textfield: Down
    //------------------------------------------------------------------------------------------
    var schoolNameTextFieldWasCreated:Bool = false
    func handleGroupTypeClick(sender:UIButton){
        
        //dropDownWasCreated ? showSchoolTextField():hideSchoolTextField()
        //showSchoolTextField()
    }
    
    func showTextField(topTextField:UITextField, textfield:UITextField, button:UIButton)
    {
        textfield.hidden = false
        button.hidden = false
        UIView.animateWithDuration(0.3,
            animations: {
                textfield.frame = CGRect(x: self.sideSpace, y: (topTextField.frame.origin.y + topTextField.frame.height + self.spacingViews), width: self.containerView.frame.width - (self.sideSpace*2) - 20, height: 30)
                textfield.alpha = 1
                button.frame = CGRect(x: (textfield.frame.origin.x + textfield.frame.width - 2), y: textfield.frame.origin.y, width: 22, height: 30)
                button.alpha = 1
        })
        
    }
    
    func hideTextField(topTextField:UITextField, textfield:UITextField, button:UIButton)
    {
        UIView.animateWithDuration(0.1,
            animations: {
                textfield.frame = CGRect(x: self.sideSpace, y: (topTextField.frame.origin.y + topTextField.frame.height + self.spacingViews), width: self.containerView.frame.width - (self.sideSpace*2) - 20, height: 30)
                textfield.alpha = 0
                button.frame = CGRect(x: (textfield.frame.origin.x + textfield.frame.width - 2), y: textfield.frame.origin.y, width: 22, height: 30)
                button.alpha = 0
            },
            completion: { finished in
                textfield.text = ""
                textfield.hidden = true
                button.hidden = true
            }
        )
    }
    //------------------------------------------------------------------------------------------
    // School Name Textfield: UP
    //------------------------------------------------------------------------------------------
    
    //------------------------------------------------------------------------------------------
    // Drop Down Group Section: Down
    //------------------------------------------------------------------------------------------
    let groupTypesDropDownView = UIView(frame: CGRectZero)
    let subGroupsDropDownView = UIView(frame: CGRectZero)
    
    var scrollView = UIScrollView()
    var textField = UITextField()
    
    var textfieldTag:Int = 0
    var dropDownTag:Int = 0
    
    // random info
    let groupTypeArray:Array<String> = ["National Youth Council","Sub group","Other groups"]
    let subGroupArray:Array<String> = ["Scouts","Red Cross","YCS","Imbuto"]
    
    @IBAction func groupTypesDropDownSelect(sender: UIButton)
    {
        switch sender{
        case groupTypeDropDownButton:
            textfieldTag = 2
            
            let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
            closeDropDownPicker(subGroupsDropDownView, textfield: textfield)
            createDropDownList(groupTypesDropDownView, textfield: textfield, array: groupTypeArray)
            openDropDownPicker(groupTypesDropDownView, textfield: textfield)
            break
        case subGroupDropDownButton:
            textfieldTag = 4
            
            let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
            closeDropDownPicker(groupTypesDropDownView, textfield: textfield)
            createDropDownList(subGroupsDropDownView, textfield: textfield, array: subGroupArray)
            openDropDownPicker(subGroupsDropDownView, textfield: textfield)
            break
        default:
            showAlertController("group drop down failed")
        }
    }
    
    var pickerHeight:CGFloat = 0
    func createDropDownList(dropDownView: UIView, textfield:UITextField, array:[String])
    {
        // Determine the picker's height
        //pickerHeight = (CGFloat)(array.count*30)
        var contentSize:CGFloat = (CGFloat)(array.count*30)
        if array.count*30 > 150 {
            pickerHeight = 150
        }
        else{
            pickerHeight = (CGFloat)(array.count*30)
        }
        if ((pickerHeight ) > (self.view.frame.height - (textfield.frame.origin.y + textfield.frame.height))){
            // create the picker drop down view
            dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y - self.pickerHeight + 65), width: textfield.frame.width, height: self.pickerHeight)
            dropDownView.layer.cornerRadius = 3
            dropDownView.alpha = 0
            dropDownView.hidden = true
            dropDownView.backgroundColor = UIColor.whiteColor()
            dropDownView.layer.borderWidth = 0.5
            dropDownView.layer.borderColor = UIColor.blackColor().CGColor
            dropDownView.userInteractionEnabled = true
        }
        else{
            // create the picker drop down view
            dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y + textfield.frame.height) - 10 , width: textfield.frame.width, height: pickerHeight)
            dropDownView.layer.cornerRadius = 3
            dropDownView.alpha = 0
            dropDownView.hidden = true
            dropDownView.backgroundColor = UIColor.whiteColor()
            dropDownView.layer.borderWidth = 0.5
            dropDownView.layer.borderColor = UIColor.blackColor().CGColor
            dropDownView.userInteractionEnabled = true
        }
        view.addSubview(dropDownView)
        
        
        // Add a scroll view to it
        scrollView.frame = CGRect(x: 0, y: 0, width: dropDownView.bounds.width, height: pickerHeight)
        //scrollView.layer.cornerRadius = 3
        scrollView.backgroundColor = UIColor.whiteColor()
        scrollView.contentSize = CGSize(width: dropDownView.bounds.width,height: contentSize)
        dropDownView.addSubview(scrollView)
        
        // Add labels for the user selection
        var offSet:CGFloat = 0
        
        for( var index = 0; index < array.count; index++){
            let label:UILabel = UILabel()
            label.frame = CGRect(x: 0, y: offSet, width: scrollView.frame.width, height: 30)
            label.layer.borderWidth = 0.25
            label.layer.borderColor = UIColor.grayColor().CGColor
            if array == groupTypeArray{
                label.tag = index+12
            }
            else {
                label.tag = index+20
            }
            label.backgroundColor = UIColor.whiteColor()
            label.text = array[index]
            label.textAlignment = NSTextAlignment.Center
            label.userInteractionEnabled = true
            
            let labelTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleLabelTap:"))
            label.addGestureRecognizer(labelTap)
            
            scrollView.addSubview(label)
            offSet += 30
        }
    }
    
    func handleLabelTap(sender:UITapGestureRecognizer){
        var tochedTag:Int = sender.view!.tag
        var textFromLabel:UILabel = UILabel()
        if tochedTag >= 10 {
            textFromLabel = self.view.viewWithTag(tochedTag) as! UILabel
        }
        
        let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
        textfield.text = textFromLabel.text
        
        if groupTypeTextField.text == "Sub group" {
            showTextField(groupTypeTextField, textfield: subGroupTextField, button: subGroupDropDownButton)
            showTextField(subGroupTextField, textfield: groupNameTextField, button: groupNameDropDownButton)
        }
        else if groupTypeTextField.text == "Other groups" {
            hideTextField(groupTypeTextField, textfield: subGroupTextField, button: subGroupDropDownButton)
            showTextField(groupTypeTextField, textfield: groupNameTextField, button: groupNameDropDownButton)
        }
        else{
            hideTextField(groupTypeTextField, textfield: subGroupTextField, button: subGroupDropDownButton)
            hideTextField(groupTypeTextField, textfield: groupNameTextField, button: groupNameDropDownButton)
        }
        
        closeDropDownPicker(groupTypesDropDownView, textfield: groupTypeTextField)
        closeDropDownPicker(subGroupsDropDownView, textfield: subGroupTextField)
    }
    
    func openDropDownPicker(dropDownView: UIView, textfield:UITextField)
    {
        dropDownView.hidden = false
        
        if ((pickerHeight + 73) > (self.view.frame.height - (textfield.frame.origin.y + textfield.frame.height))){
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y - self.pickerHeight + 65), width: textfield.frame.width, height: self.pickerHeight)
                    dropDownView.alpha = 1
            })
        }
        else{
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y + textfield.frame.height + 65), width: textfield.frame.width, height: self.pickerHeight)
                    dropDownView.alpha = 1
            })
        }
    }
    
    func closeDropDownPicker(dropDownView: UIView, textfield:UITextField)
    {
        if ((pickerHeight ) > (self.view.frame.height - (textfield.frame.origin.y + textfield.frame.height))){
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y - self.pickerHeight + 65) + 10, width: textfield.frame.width, height: self.pickerHeight)
                    dropDownView.alpha = 0
                },
                completion: { finished in
                    dropDownView.hidden = true
                }
            )
        }
        else{
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y + textfield.frame.height) - 10 , width: textfield.frame.width, height: dropDownView.frame.height)
                    dropDownView.alpha = 0
                },
                completion: { finished in
                    dropDownView.hidden = true
                }
            )
        }
    }
    
    //------------------------------------------------------------------------------------------
    // Drop Down Group Section: UP
    //------------------------------------------------------------------------------------------
    
    func continueButtonClick(sender:UIButton){
        if !groupTypeTextField.text.isEmpty {
            if groupTypeTextField.text == groupTypeArray[0] {
                self.user_affiliation = groupTypeTextField.text
                self.user_group = groupTypeTextField.text
                self.user_subgroup = "none"
                performSegueWithIdentifier("showLeaderAddNYCGroupPage", sender: self)
            }
            else{
                if subGroupTextField.hidden == false {
                    self.user_affiliation = groupTypeTextField.text
                    self.user_group = groupNameTextField.text
                    self.user_subgroup = subGroupTextField.text
                }
                else{
                    self.user_affiliation = groupTypeTextField.text
                    self.user_group = groupNameTextField.text
                    self.user_subgroup = "none"
                }
                performSegueWithIdentifier("showLeaderAddNYCGroupPage", sender: self)
            }
        }
        else{
            showAlertController("Fill the required info please!")
        }
        
        
    }
    
    func showAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    var kbHeight: CGFloat!
    var textFieldTag: Int = -1
    
    func animateTextField(up: Bool) {
        var movement = (up ? -kbHeight : kbHeight)
        
        UIView.animateWithDuration(0.3, animations: {
            self.containerView.frame = CGRectOffset(self.containerView.frame, 0, movement)
        })
    }
    
    override func viewWillAppear(animated:Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                var textfield:UITextField = UITextField()
                if self.view.viewWithTag(textFieldTag) is UITextField{
                    textfield = self.view.viewWithTag(textFieldTag) as! UITextField
                }
                if((keyboardSize.height + 25) > (self.containerView.frame.height - textfield.frame.origin.y)){
                    kbHeight = (keyboardSize.height - (self.containerView.frame.height - textfield.frame.origin.y)) + 35
                }
                else{
                    kbHeight = 0
                }
                self.animateTextField(true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.animateTextField(false)
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "About"],
            ["title" : "Settings"],
            ["title" : "Share"],
            ["title" : "Sign In"]
        ]
    }
    
    @IBAction func pickerSelect(sender: UIBarButtonItem) {
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createPicker()
    {
        pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        pickerMenu.layer.borderWidth = 0.75
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func openPicker() {
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker() {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    func buttonClick(sender: UIButton) {
        sender.backgroundColor = UIColor(red: (33/360), green: (47/360), blue: (61/360), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        closePicker()
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        textFieldTag = textField.tag
        return true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        closeDropDownPicker(groupTypesDropDownView, textfield: groupTypeTextField)
        closeDropDownPicker(subGroupsDropDownView, textfield: subGroupTextField)
        
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showLeaderAddNYCGroupPage" {
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! addGroupNYCViewController
            toViewController.iphoneType = self.iphoneType!
            toViewController.transitioningDelegate = self.transitionManager
            
            // transfer user data
            toViewController.user_email = self.user_email!
            toViewController.user_password = self.user_password!
            toViewController.user_gender = self.user_gender!
            toViewController.user_status = self.user_status!
            toViewController.user_first_name = self.user_first_name!
            toViewController.user_last_name = self.user_last_name!
            toViewController.user_phone_number = self.user_phone_number!
            toViewController.user_affiliation = self.user_affiliation!
            toViewController.user_group = self.user_group!
            toViewController.user_subgroup = self.user_subgroup!
            
        }
    }

    
    // unwind the segue
    @IBAction func handleBackButtonClick(sender:UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
