//
//  registerViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 5/15/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class registerViewController: UIViewController,UITextFieldDelegate {
    
    // -------------- user data-----------------
    var user_name:String?
    var user_first_name:String?
    var user_last_name:String?
    var user_gender:String?
    var user_status:String?
    var user_image:UIImage?
    var user_phone_number:String?
    var user_email:String?
    var user_password:String?
    var user_group:String?
    var user_subgroup:String?
    var user_affiliation:String?
    var user_province:String?
    var user_district:String?
    var user_sector:String?
    // -------------- user data-----------------
    
    // type of iphone
    var iphoneType:Int?
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    
    @IBOutlet weak var mainNavBar: UINavigationBar!
    var containerView:UIView = UIView()
    
    // Screen Properties
    var spacingViews:CGFloat = 4
    let sideSpace:CGFloat = 40
    
    // Outlets
    var youthConnektLogo:UIImageView = UIImageView()
    var emailTextField:UITextField = UITextField()
    var passwordTextField:UITextField = UITextField()
    var confirmPasswordText:UITextField = UITextField()
    
    var genderTextField:UITextField = UITextField()
    var genderDropDownButton:UIButton = UIButton()
    
    var signInButton:UIButton = UIButton()
    var signUpButton:UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        createViews(iphoneType!, containerViewHeight: (self.view.frame.height - (mainNavBar.frame.origin.y + mainNavBar.frame.height)),containerViewWidth: self.view.frame.width)
        createPicker()
        createDropDownGender()
    }
    
    func createViews(typeOfIphone:Int, containerViewHeight:CGFloat, containerViewWidth:CGFloat)
    {
        var youthConnektLogoSize:CGFloat = 0
        var spaceLogo_NavBar:CGFloat = 0
        var middlePartOffset:CGFloat = 0
        
        switch typeOfIphone{
        case 4:
            youthConnektLogoSize = 90
            spaceLogo_NavBar += 10
            middlePartOffset = 10
            spacingViews = 4
            break
        case 5:
            youthConnektLogoSize = 120
            spaceLogo_NavBar += 15
            middlePartOffset = 20
            spacingViews = 8
            break
        case 6:
            youthConnektLogoSize = 150
            spaceLogo_NavBar += 35
            middlePartOffset = 20
            spacingViews = 12
            break
        case 61:
            youthConnektLogoSize = 150
            spaceLogo_NavBar += 35
            middlePartOffset = 20
            spacingViews = 12
            
            break
        default:
            println("choose Iphone!")
        }
        
        containerView.frame = CGRect(x: 0, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height), width: containerViewWidth, height: containerViewHeight)
        containerView.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        self.view.addSubview(containerView)
        
        emailTextField.frame = CGRect(x: sideSpace, y: (containerView.frame.height/2 - 80), width: containerView.frame.width - (sideSpace*2), height: 30)
        emailTextField.delegate = self
        emailTextField.tag = 1
        emailTextField.placeholder = "email"
        emailTextField.textColor = UIColor.whiteColor()
        
        var emailBorder = CALayer()
        var width = CGFloat(0.75)
        emailBorder.borderColor = UIColor.whiteColor().CGColor
        emailBorder.borderWidth = width
        emailBorder.frame = CGRectMake(0, emailTextField.frame.size.height - width, emailTextField.frame.size.width, emailTextField.frame.size.height)
        emailTextField.layer.addSublayer(emailBorder)
        emailTextField.layer.masksToBounds = true
        
        containerView.addSubview(emailTextField)
        
        youthConnektLogo.frame = CGRect(x: ((containerView.frame.width / 2) - (youthConnektLogoSize/2)), y: spaceLogo_NavBar + spaceLogo_NavBar, width: youthConnektLogoSize , height: youthConnektLogoSize)
        youthConnektLogo.image = UIImage(named:"YouthConnekt_Logo")
        containerView.addSubview(youthConnektLogo)
        
        
        passwordTextField.frame = CGRect(x: sideSpace, y: containerView.frame.height/2 - 30, width: containerView.frame.width - (sideSpace*2), height: 30)
        passwordTextField.delegate = self
        
        passwordTextField.tag = 2
        passwordTextField.placeholder = "password"
        passwordTextField.secureTextEntry = true
        passwordTextField.textColor = UIColor.whiteColor()
        
        var passwordBorder = CALayer()
        passwordBorder.borderColor = UIColor.whiteColor().CGColor
        passwordBorder.borderWidth = width
        passwordBorder.frame = CGRectMake(0, passwordTextField.frame.size.height - width, passwordTextField.frame.size.width, passwordTextField.frame.size.height)
        passwordTextField.layer.addSublayer(passwordBorder)
        passwordTextField.layer.masksToBounds = true
        
        containerView.addSubview(passwordTextField)
        
        confirmPasswordText.frame = CGRect(x: sideSpace, y: (containerView.frame.height/2 + 20), width: containerView.frame.width - (sideSpace*2), height: 30)
        confirmPasswordText.delegate = self
        
        confirmPasswordText.tag = 3
        confirmPasswordText.placeholder = "confirm password"
        confirmPasswordText.secureTextEntry = true
        confirmPasswordText.textColor = UIColor.whiteColor()
        
        var confirmPasswordBorder = CALayer()
        confirmPasswordBorder.borderColor = UIColor.whiteColor().CGColor
        confirmPasswordBorder.borderWidth = width
        confirmPasswordBorder.frame = CGRectMake(0, confirmPasswordText.frame.size.height - width, confirmPasswordText.frame.size.width, confirmPasswordText.frame.size.height)
        confirmPasswordText.layer.addSublayer(confirmPasswordBorder)
        confirmPasswordText.layer.masksToBounds = true
        
        containerView.addSubview(confirmPasswordText)
        
        genderTextField.frame = CGRect(x: sideSpace, y: (confirmPasswordText.frame.origin.y + confirmPasswordText.frame.height + 20), width: containerView.frame.width - (sideSpace*2 + 20), height: 30)
        genderTextField.delegate = self
        genderTextField.userInteractionEnabled = false
        
        genderTextField.tag = 4
        genderTextField.placeholder = "gender"
        genderTextField.textColor = UIColor.whiteColor()
        
        var genderTextBorder = CALayer()
        genderTextBorder.borderColor = UIColor.whiteColor().CGColor
        genderTextBorder.borderWidth = width
        genderTextBorder.frame = CGRectMake(0, genderTextField.frame.size.height - width, genderTextField.frame.size.width, genderTextField.frame.size.height)
        genderTextField.layer.addSublayer(genderTextBorder)
        genderTextField.layer.masksToBounds = true
        
        containerView.addSubview(genderTextField)
        
        // drop down button
        genderDropDownButton.frame = CGRect(x: (genderTextField.frame.origin.x + genderTextField.frame.width - 2) , y: genderTextField.frame.origin.y, width: 22, height: 30)
        genderDropDownButton.layer.cornerRadius = 2
        genderDropDownButton.setImage(UIImage(named: "dropDown-50"), forState: UIControlState.Normal)
        genderDropDownButton.addTarget(self, action: "pickGender:", forControlEvents: UIControlEvents.TouchUpInside)
        var genderDropDownBorder = CALayer()
        genderDropDownBorder.borderColor = UIColor.whiteColor().CGColor
        genderDropDownBorder.borderWidth = width
        genderDropDownBorder.frame = CGRectMake(0, genderDropDownButton.frame.size.height - width, genderDropDownButton.frame.size.width, genderDropDownButton.frame.size.height)
        genderDropDownButton.layer.addSublayer(genderDropDownBorder)
        genderDropDownButton.layer.masksToBounds = true
        
        containerView.addSubview(genderDropDownButton)
        
        signUpButton.frame = CGRect(x: (containerView.frame.width/2 - 64), y: (confirmPasswordText.frame.origin.y + confirmPasswordText.frame.height + 70), width: 128, height: 30)
        signUpButton.tag = 5
        signUpButton.layer.borderColor = UIColor.whiteColor().CGColor
        signUpButton.layer.borderWidth = 1
        signUpButton.layer.cornerRadius = 3
        signUpButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        signUpButton.setTitle("Sign Up", forState: .Normal)
        signUpButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        signUpButton.titleLabel?.textAlignment = NSTextAlignment.Center
        signUpButton.addTarget(self, action: "signUpButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(signUpButton)
        
        signInButton.frame = CGRect(x: (containerView.frame.width/2 - 105), y: (signUpButton.frame.origin.y + signUpButton.frame.height), width: 210, height: 30)
        signInButton.tag = 6
        signInButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        signInButton.setTitle("Already a member? Sign In", forState: .Normal)
        signInButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        signInButton.titleLabel?.textAlignment = NSTextAlignment.Center
        signInButton.addTarget(self, action: "signInButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(signInButton)
    }
    
    //------------------------------------------------------------------------------------------
    // Gender Section
    //------------------------------------------------------------------------------------------
    let genderMenu = UIView(frame: CGRectZero)
    
    struct genderBtnStruct {
        static let btnProperties = [
            ["title" : "male"],
            ["title" : "female"],
        ]
    }
    
    func pickGender(sender: UIButton)
    {
        closePicker()
        genderMenu.hidden ? openGenderPicker() : closeGenderPicker()
    }
    
    func createDropDownGender()
    {
        genderMenu.frame = CGRect(x: (self.genderTextField.frame.origin.x), y: (self.genderTextField.frame.origin.y + genderTextField.frame.height + 1), width: genderTextField.frame.width, height: 60)
        genderMenu.alpha = 0
        genderMenu.hidden = true
        genderMenu.backgroundColor = UIColor.whiteColor()
        genderMenu.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        genderMenu.layer.borderWidth = 0.75
        genderMenu.userInteractionEnabled = true
        
        var offset:CGFloat = 0
        for (index, name) in enumerate(genderBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: genderMenu.frame.width, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "genderBtnClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            genderMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(genderMenu)
    }
    
    func genderBtnClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/255), green: (47/255), blue: (61/255), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.genderTextField.text = sender.titleLabel?.text
        closeGenderPicker()
    }
    
    func openGenderPicker()
    {
        self.genderMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: self.genderTextField.frame.origin.x, y: (self.genderTextField.frame.origin.y + self.genderTextField.frame.height + 1), width: self.genderTextField.frame.width, height: 60)
                self.genderMenu.alpha = 1
        })
    }
    
    func closeGenderPicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: self.genderTextField.frame.origin.x, y: (self.genderTextField.frame.origin.y + self.genderTextField.frame.height + 1), width: self.genderTextField.frame.width, height: 60)
                
                self.genderMenu.alpha = 0
            },
            completion: { finished in
                self.genderMenu.hidden = true
            }
        )
    }
    
    //------------------------------------------------------------------------------------------
    // Gender Section
    //------------------------------------------------------------------------------------------
    
    // button click handlers
    func signUpButtonClick(sender:UIButton){
        closePicker()
        if !emailTextField.text.isEmail && passwordTextField.text.isEmpty {
            showAlertController("Please enter your email and your password!")
        }
        else if passwordTextField.text != confirmPasswordText.text {
            showAlertController("Password did not match!")
        }
        else if !emailTextField.text.isEmail{
            showAlertController("Please enter your correct email!")
        }
        else {
            // ============ Segue to the next page ================
            
            self.user_email = emailTextField.text
            self.user_password = confirmPasswordText.text
            self.user_gender = genderTextField.text
            
            performSegueWithIdentifier("chooseCategory", sender: self)
        }
    }
    
    func signInButtonClick(sender:UIButton){
        closePicker()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func showAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == emailTextField{
            if !emailTextField.text.isEmail{
                emailTextField.textColor = UIColor.redColor()
            }
            else{
                emailTextField.textColor = UIColor.whiteColor()
            }
        }
        else if textField == confirmPasswordText{
            if passwordTextField.text.isEmpty{
                showAlertController("Enter in your password first!")
            }
            else if passwordTextField.text != confirmPasswordText.text{
                showAlertController("Password did not match!")
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    var kbHeight: CGFloat!
    var textFieldTag: Int = -1
    
    func animateTextField(up: Bool) {
        var movement = (up ? -kbHeight : kbHeight)
        
        UIView.animateWithDuration(0.3, animations: {
            self.containerView.frame = CGRectOffset(self.containerView.frame, 0, movement)
        })
    }
    
    override func viewWillAppear(animated:Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                let textfield:UITextField = self.view.viewWithTag(textFieldTag) as! UITextField
                if((keyboardSize.height + 25) > (self.containerView.frame.height - textfield.frame.origin.y)){
                    kbHeight = (keyboardSize.height - (self.containerView.frame.height - textfield.frame.origin.y)) + 35
                    
                }
                else{
                    kbHeight = 0
                }
                self.animateTextField(true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.animateTextField(false)
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "About"],
            ["title" : "Settings"],
            ["title" : "Share"],
            ["title" : "Sign In"]
        ]
    }
    
    @IBAction func pickerSelect(sender: UIBarButtonItem)
    {
        closeGenderPicker()
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createPicker()
    {
        pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func openPicker()
    {
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    func buttonClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/360), green: (47/360), blue: (61/360), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        switch sender.tag{
        case 0: // About
            performSegueWithIdentifier("fromRegisterToAbout", sender: self)
            break
        case 1: // Settings
            performSegueWithIdentifier("fromRegisterToSettings", sender: self)
            break
        case 2: // Share
            performSegueWithIdentifier("fromRegisterToShare", sender: self)
            break
        case 3: // Sign In
            self.dismissViewControllerAnimated(true, completion: nil)
            break
        default:
            println("No such segue exists in your app!")
        }
        
        closePicker()
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        textFieldTag = textField.tag
        closePicker()
        return true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        closePicker()
        closeGenderPicker()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // unwind the segue
    @IBAction func handleBackButtonClick(sender:UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "loginSegue" {
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! LoginViewController
            
            toViewController.transitioningDelegate = self.transitionManager
            
        }
        else if segue.identifier == "chooseCategory" {
            let toViewController = segue.destinationViewController as! welcomeIstPageViewController
            toViewController.iphoneType = self.iphoneType!
            
            // transfer user data
            toViewController.user_email = self.user_email!
            toViewController.user_password = self.user_password!
            toViewController.user_gender = self.user_gender!
            
        }
    }
}

