//
//  customUIView.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 5/15/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class customUIView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 5.0
        layer.borderColor = UIColor(hue: 0.999, saturation: 0, brightness: 0.97, alpha: 1).CGColor
        self.layer.borderWidth = 1
        self.backgroundColor = UIColor(hue: 1, saturation: 0, brightness: 1, alpha: 0.2)
    }

}
