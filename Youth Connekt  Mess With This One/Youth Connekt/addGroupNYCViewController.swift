//
//  addGroupNYCViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 6/7/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class addGroupNYCViewController: UIViewController,UITextFieldDelegate {
    
    // -------------- user data-----------------
    var user_name:String?
    var user_first_name:String?
    var user_last_name:String?
    var user_gender:String?
    var user_status:String?
    var user_image:UIImage?
    var user_phone_number:String?
    var user_email:String?
    var user_password:String?
    var user_group:String?
    var user_subgroup:String?
    var user_affiliation:String?
    var user_province:String?
    var user_district:String?
    var user_sector:String?
    // -------------- user data-----------------
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    
    @IBOutlet weak var mainNavBar: UINavigationBar!
    var containerView:UIView = UIView()
    
    // Screen Properties
    var spacingViews:CGFloat = 4
    let sideSpace:CGFloat = 40
    
    // Outlets
    var titleLabel:UILabel = UILabel()
    
    var chooseLevelTextField:UITextField = UITextField()
    var chooseLevelDropDownButton:UIButton = UIButton()
    
    var provinceTextField:UITextField = UITextField()
    var provinceDropDownButton:UIButton = UIButton()
    
    var districtTextField:UITextField = UITextField()
    var districtDropDownButton:UIButton = UIButton()
    
    var sectorTextField:UITextField = UITextField()
    var sectorDropDownButton:UIButton = UIButton()
    
    var addMyGroupButton:UIButton = UIButton()
    
    // type of iphone
    var iphoneType:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        createViews(iphoneType!, containerViewHeight: (self.view.frame.height - (mainNavBar.frame.origin.y + mainNavBar.frame.height)),containerViewWidth: self.view.frame.width)
        createPicker()
        
        createProvinceTextField()
        createDistrictTextField()
        createSectorTextField()
    }
    
    func createViews(typeOfIphone:Int, containerViewHeight:CGFloat, containerViewWidth:CGFloat)
    {
        var youthConnektLogoSize:CGFloat = 0
        var spaceLogo_NavBar:CGFloat = 0
        var middlePartOffset:CGFloat = 0
        
        switch typeOfIphone{
        case 4:
            spacingViews = 20
            break
        case 5:
            spacingViews = 30
            break
        case 6:
            spacingViews = 50
            break
        case 61:
            spacingViews = 60
            
            break
        default:
            spacingViews = 10
        }
        
        containerView.frame = CGRect(x: 0, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height), width: containerViewWidth, height: containerViewHeight)
        containerView.backgroundColor = UIColor(red: (232/255), green: (227/255), blue: (216/255), alpha: 1)
        self.view.addSubview(containerView)
        
        //============================ titleLabel ===================================
        titleLabel.frame = CGRect(x: ((containerView.frame.width / 2) - 150), y: spacingViews, width: 300, height: 30)
        titleLabel.tag = 1
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.font = UIFont.systemFontOfSize(20, weight: 0.1)
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.text = "Group Location"
        
        containerView.addSubview(titleLabel)
        
        
        //============================ provinceTextField ===================================
        chooseLevelTextField.frame = CGRect(x: sideSpace, y: (titleLabel.frame.origin.y + titleLabel.frame.height + (spacingViews*2)), width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        chooseLevelTextField.userInteractionEnabled = false
        chooseLevelTextField.delegate = self
        chooseLevelTextField.tag = 2
        chooseLevelTextField.placeholder = "choose level"
        chooseLevelTextField.textColor = UIColor.blueColor()
        UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        var levelBorder = CALayer()
        var width = CGFloat(0.75)
        levelBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        levelBorder.borderWidth = width
        levelBorder.frame = CGRectMake(0, chooseLevelTextField.frame.size.height - width, chooseLevelTextField.frame.size.width, chooseLevelTextField.frame.size.height)
        chooseLevelTextField.layer.addSublayer(levelBorder)
        chooseLevelTextField.layer.masksToBounds = true
        
        containerView.addSubview(chooseLevelTextField)
        
        chooseLevelDropDownButton.frame = CGRect(x: (chooseLevelTextField.frame.origin.x + chooseLevelTextField.frame.width - 2), y: chooseLevelTextField.frame.origin.y, width: 22, height: 30)
        chooseLevelDropDownButton.tag = 3
        chooseLevelDropDownButton.layer.cornerRadius = 3
        chooseLevelDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        var levelDropDownBorder = CALayer()
        levelDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        levelDropDownBorder.borderWidth = width
        levelDropDownBorder.frame = CGRectMake(0, chooseLevelDropDownButton.frame.size.height - width, chooseLevelDropDownButton.frame.size.width, chooseLevelDropDownButton.frame.size.height)
        chooseLevelDropDownButton.layer.addSublayer(levelDropDownBorder)
        chooseLevelDropDownButton.layer.masksToBounds = true
        chooseLevelDropDownButton.addTarget(self, action: "dropDownSelect:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(chooseLevelDropDownButton)
        
        addMyGroupButton.frame = CGRect(x: (containerView.frame.width/2 - 60), y: (containerView.frame.height - 100), width: 120, height: 30)
        addMyGroupButton.tag = 10
        addMyGroupButton.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        addMyGroupButton.layer.borderWidth = 1
        addMyGroupButton.layer.cornerRadius = 3
        addMyGroupButton.setTitleColor(UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1), forState: .Normal)
        addMyGroupButton.setTitle("Add Group", forState: .Normal)
        addMyGroupButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        addMyGroupButton.titleLabel?.textAlignment = NSTextAlignment.Center
        addMyGroupButton.addTarget(self, action: "addGroupButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(addMyGroupButton)
        
    }
    
    func createProvinceTextField(){
        //============================ provinceTextField ===================================
        provinceTextField.frame = CGRect(x: sideSpace, y: (titleLabel.frame.origin.y + titleLabel.frame.height + (spacingViews*3) + chooseLevelTextField.frame.height), width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        provinceTextField.userInteractionEnabled = false
        provinceTextField.delegate = self
        provinceTextField.tag = 4
        provinceTextField.placeholder = "province"
        provinceTextField.textColor = UIColor.blueColor()
        
        var provinceBorder = CALayer()
        var width = CGFloat(0.75)
        provinceBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        provinceBorder.borderWidth = width
        provinceBorder.frame = CGRectMake(0, provinceTextField.frame.size.height - width, provinceTextField.frame.size.width, provinceTextField.frame.size.height)
        provinceTextField.layer.addSublayer(provinceBorder)
        provinceTextField.layer.masksToBounds = true
        
//        provinceTextField.alpha = 0
//        provinceTextField.hidden = true
        
        containerView.addSubview(provinceTextField)
        
        provinceDropDownButton.frame = CGRect(x: (provinceTextField.frame.origin.x + provinceTextField.frame.width - 2), y: provinceTextField.frame.origin.y, width: 22, height: 30)
        provinceDropDownButton.tag = 5
        provinceDropDownButton.layer.cornerRadius = 3
        provinceDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        var provinceDropDownBorder = CALayer()
        provinceDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        provinceDropDownBorder.borderWidth = width
        provinceDropDownBorder.frame = CGRectMake(0, provinceDropDownButton.frame.size.height - width, provinceDropDownButton.frame.size.width, provinceDropDownButton.frame.size.height)
        provinceDropDownButton.layer.addSublayer(provinceDropDownBorder)
        provinceDropDownButton.layer.masksToBounds = true
        provinceDropDownButton.addTarget(self, action: "dropDownSelect:", forControlEvents: UIControlEvents.TouchUpInside)
        
//        provinceDropDownButton.alpha = 0
//        provinceDropDownButton.hidden = true
        
        containerView.addSubview(provinceDropDownButton)
        //============================ provinceTextField ===================================
    }
    
    func createDistrictTextField(){
        //============================ districtTextField ===================================
        districtTextField.frame = CGRect(x: sideSpace, y: (titleLabel.frame.origin.y + titleLabel.frame.height + (spacingViews*4) + chooseLevelTextField.frame.height + 30) , width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        districtTextField.userInteractionEnabled = false
        districtTextField.delegate = self
        districtTextField.tag = 6
        districtTextField.placeholder = "district"
        districtTextField.textColor = UIColor.blueColor()
        
        var districtBorder = CALayer()
        var width = CGFloat(0.75)
        districtBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        districtBorder.borderWidth = width
        districtBorder.frame = CGRectMake(0, districtTextField.frame.size.height - width, districtTextField.frame.size.width, districtTextField.frame.size.height)
        districtTextField.layer.addSublayer(districtBorder)
        districtTextField.layer.masksToBounds = true
        
//        districtTextField.alpha = 0
//        districtTextField.hidden = true
        
        containerView.addSubview(districtTextField)
        
        districtDropDownButton.frame = CGRect(x: (districtTextField.frame.origin.x + districtTextField.frame.width - 2), y: districtTextField.frame.origin.y, width: 22, height: 30)
        districtDropDownButton.tag = 7
        districtDropDownButton.layer.cornerRadius = 3
        districtDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        var districtDropDownBorder = CALayer()
        districtDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        districtDropDownBorder.borderWidth = width
        districtDropDownBorder.frame = CGRectMake(0, districtDropDownButton.frame.size.height - width, districtDropDownButton.frame.size.width, districtDropDownButton.frame.size.height)
        districtDropDownButton.layer.addSublayer(districtDropDownBorder)
        districtDropDownButton.layer.masksToBounds = true
        districtDropDownButton.addTarget(self, action: "dropDownSelect:", forControlEvents: UIControlEvents.TouchUpInside)
        
//        districtDropDownButton.alpha = 0
//        districtDropDownButton.hidden = true
        
        containerView.addSubview(districtDropDownButton)
        //============================ districtTextField ===================================
    }
    
    func createSectorTextField(){
        //============================ sectorTextField ===================================
        sectorTextField.frame = CGRect(x: sideSpace, y: (titleLabel.frame.origin.y + titleLabel.frame.height + (spacingViews*5) + chooseLevelTextField.frame.height + (2*30)) , width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        sectorTextField.userInteractionEnabled = false
        sectorTextField.delegate = self
        sectorTextField.tag = 8
        sectorTextField.placeholder = "sector"
        sectorTextField.textColor = UIColor.blueColor()
        
        var sectorBorder = CALayer()
        var width = CGFloat(0.75)
        sectorBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        sectorBorder.borderWidth = width
        sectorBorder.frame = CGRectMake(0, sectorTextField.frame.size.height - width, sectorTextField.frame.size.width, sectorTextField.frame.size.height)
        sectorTextField.layer.addSublayer(sectorBorder)
        sectorTextField.layer.masksToBounds = true
        
//        sectorTextField.alpha = 0
//        sectorTextField.hidden = true
        
        containerView.addSubview(sectorTextField)
        
        sectorDropDownButton.frame = CGRect(x: (sectorTextField.frame.origin.x + sectorTextField.frame.width - 2), y: sectorTextField.frame.origin.y, width: 22, height: 30)
        sectorDropDownButton.tag = 9
        sectorDropDownButton.layer.cornerRadius = 3
        sectorDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        var sectorDropDownBorder = CALayer()
        sectorDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        sectorDropDownBorder.borderWidth = width
        sectorDropDownBorder.frame = CGRectMake(0, sectorDropDownButton.frame.size.height - width, sectorDropDownButton.frame.size.width, sectorDropDownButton.frame.size.height)
        sectorDropDownButton.layer.addSublayer(sectorDropDownBorder)
        sectorDropDownButton.layer.masksToBounds = true
        sectorDropDownButton.addTarget(self, action: "dropDownSelect:", forControlEvents: UIControlEvents.TouchUpInside)
        
//        sectorDropDownButton.alpha = 0
//        sectorDropDownButton.hidden = true
        
        containerView.addSubview(sectorDropDownButton)
        //============================ sectorTextField ===================================
    }
    
    func showTextField(topTextField:UITextField, textfield:UITextField, button:UIButton)
    {
        textfield.hidden = false
        button.hidden = false
        UIView.animateWithDuration(0.3,
            animations: {
                textfield.frame = CGRect(x: self.sideSpace, y: (topTextField.frame.origin.y + topTextField.frame.height + self.spacingViews), width: self.containerView.frame.width - (self.sideSpace*2) - 20, height: 30)
                textfield.alpha = 1
                button.frame = CGRect(x: (textfield.frame.origin.x + textfield.frame.width - 2), y: textfield.frame.origin.y, width: 22, height: 30)
                button.alpha = 1
        })
        
    }
    
    func hideTextField(topTextField:UITextField, textfield:UITextField, button:UIButton)
    {
        UIView.animateWithDuration(0.1,
            animations: {
                textfield.frame = CGRect(x: self.sideSpace, y: (topTextField.frame.origin.y + topTextField.frame.height + self.spacingViews), width: self.containerView.frame.width - (self.sideSpace*2) - 20, height: 30)
                textfield.alpha = 0
                button.frame = CGRect(x: (textfield.frame.origin.x + textfield.frame.width - 2), y: textfield.frame.origin.y, width: 22, height: 30)
                button.alpha = 0
            },
            completion: { finished in
                textfield.text = ""
                textfield.hidden = true
                button.hidden = true
            }
        )
    }
    
    // button click handlers
    func addGroupButtonClick(sender:UIButton){
        if provinceTextField.text.isEmpty||districtTextField.text.isEmpty||sectorTextField.text.isEmpty||chooseLevelTextField.text.isEmpty{
            showAlertController("Please fill all required information")
        }
        else{
            self.user_province = provinceTextField.text
            self.user_district = districtTextField.text
            self.user_sector = sectorTextField.text
            
            performSegueWithIdentifier("goToNYCWelcomePage", sender: self)
        }
    }
    
    func showAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "About"],
            ["title" : "Settings"],
            ["title" : "Share"],
            ["title" : "Sign In"]
        ]
    }
    
    @IBAction func pickerSelect(sender: UIBarButtonItem)
    {
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createPicker()
    {
        pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        pickerMenu.layer.borderWidth = 0.75
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func openPicker()
    {
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    func buttonClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/360), green: (47/360), blue: (61/360), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        switch sender.tag{
        case 0: // About
            performSegueWithIdentifier("fromNYCToAbout", sender: self)
            break
        case 1: // Settings
            performSegueWithIdentifier("fromNYCToSettings", sender: self)
            break
        case 2: // Share
            performSegueWithIdentifier("fromNYCToShare", sender: self)
            break
        case 3: // Sign In
            
            break
        default:
            println("No such segue exists in your app!")
        }
        
        closePicker()
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    
    //------------------------------------------------------------------------------------------
    // Drop Down Section
    //------------------------------------------------------------------------------------------
    let levelDropDownView = UIView(frame: CGRectZero)
    let provinceDropDownView = UIView(frame: CGRectZero)
    let districtDropDownView = UIView(frame: CGRectZero)
    let sectorDropDownView = UIView(frame: CGRectZero)
    
    var scrollView = UIScrollView()
    var textField = UITextField()
    
    // textfield tags
    var textfieldTag:Int = 0
    var dropDownTag:Int = 0
    
    // random info
    var levelArray:Array<String> = ["Country level","Province level","District level","Sector level"]
    var provinceArray:Array<String> = ["Northern province","Southern province","Kigali city","Eastern province","Western province"]
    var districtArray:Array<String> = ["Gasabo","Nyarugenge","Kicukiro"]
    var sectorArray:Array<String> = ["Gatsata","Kinyinya","Jabana"]
    
    func dropDownSelect(sender: UIButton)
    {
        switch sender{
        case chooseLevelDropDownButton:
            textfieldTag = 2
            dropDownTag = 21
            
            closeDropDownPicker(provinceDropDownView, textfield: provinceTextField)
            closeDropDownPicker(districtDropDownView, textfield: districtTextField)
            closeDropDownPicker(sectorDropDownView, textfield: sectorTextField)
            
            let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
            createDropDownList(levelDropDownView, textfield: textfield, array: levelArray)
            openDropDownPicker(levelDropDownView, textfield: textfield)
            break
        case provinceDropDownButton:
            textfieldTag = 4
            dropDownTag = 22
            
            closeDropDownPicker(levelDropDownView, textfield: chooseLevelTextField)
            closeDropDownPicker(districtDropDownView, textfield: districtTextField)
            closeDropDownPicker(sectorDropDownView, textfield: sectorTextField)
            
            let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
            createDropDownList(provinceDropDownView, textfield: textfield, array: provinceArray)
            openDropDownPicker(provinceDropDownView, textfield: textfield)
            break
        case districtDropDownButton:
            textfieldTag = 6
            dropDownTag = 23
            
            closeDropDownPicker(levelDropDownView, textfield: chooseLevelTextField)
            closeDropDownPicker(provinceDropDownView, textfield: provinceTextField)
            closeDropDownPicker(sectorDropDownView, textfield: sectorTextField)
            
            let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
            createDropDownList(districtDropDownView, textfield: textfield, array: districtArray)
            openDropDownPicker(districtDropDownView, textfield: textfield)
            break
        case sectorDropDownButton:
            textfieldTag = 8
            dropDownTag = 24
            
            closeDropDownPicker(levelDropDownView, textfield: chooseLevelTextField)
            closeDropDownPicker(provinceDropDownView, textfield: provinceTextField)
            closeDropDownPicker(districtDropDownView, textfield: districtTextField)
            
            let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
            createDropDownList(sectorDropDownView, textfield: textfield, array: sectorArray)
            openDropDownPicker(sectorDropDownView, textfield: textfield)
            break
        default:
            println("In the drop down select!")
        }
    }
    
    var pickerHeight:CGFloat = 0
    func createDropDownList(dropDownView: UIView, textfield:UITextField, array:[String])
    {
        var contentSize:CGFloat = (CGFloat)(array.count*30)
        if array.count*30 > 150 {
            pickerHeight = 150
        }
        else{
            pickerHeight = (CGFloat)(array.count*30)
        }
        
        if ((pickerHeight + 73) > (self.view.frame.height - (textfield.frame.origin.y + textfield.frame.height))){
            dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y - pickerHeight) + 10, width: textfield.frame.width, height: pickerHeight)
            
            if (dropDownView == self.levelDropDownView){
                dropDownView.tag = 21
            }
            else if (dropDownView == self.provinceDropDownView){
                dropDownView.tag = 22
            }
            else if (dropDownView == self.districtDropDownView){
                dropDownView.tag = 23
            }
            else{
                dropDownView.tag = 24
            }
            
            dropDownView.layer.cornerRadius = 3
            dropDownView.alpha = 0
            dropDownView.hidden = true
            dropDownView.backgroundColor = UIColor.whiteColor()
            dropDownView.layer.borderWidth = 0.5
            dropDownView.layer.borderColor = UIColor.blackColor().CGColor
            dropDownView.userInteractionEnabled = true
            view.addSubview(dropDownView)
            
        }
        else{
            
            // create the picker drop down view
            dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y + textfield.frame.height) - 10 , width: textfield.frame.width, height: pickerHeight)
            
            if (dropDownView == self.levelDropDownView){
                dropDownView.tag = 21
            }
            else if (dropDownView == self.provinceDropDownView){
                dropDownView.tag = 22
            }
            else if (dropDownView == self.districtDropDownView){
                dropDownView.tag = 23
            }
            else{
                dropDownView.tag = 24
            }
            
            dropDownView.layer.cornerRadius = 3
            dropDownView.alpha = 0
            dropDownView.hidden = true
            dropDownView.backgroundColor = UIColor.whiteColor()
            dropDownView.layer.borderWidth = 0.5
            dropDownView.layer.borderColor = UIColor.blackColor().CGColor
            dropDownView.userInteractionEnabled = true
            view.addSubview(dropDownView)
        }
        
        // Add a scroll view to it
        scrollView.frame = CGRect(x: 0, y: 0, width: dropDownView.bounds.width, height: pickerHeight)
        //scrollView.layer.cornerRadius = 3
        scrollView.backgroundColor = UIColor.whiteColor()
        scrollView.contentSize = CGSize(width: dropDownView.bounds.width, height: contentSize)
        dropDownView.addSubview(scrollView)
        
        // Add labels for the user selection
        var offSet:CGFloat = 0
        
        for( var index = 0; index < array.count; index++){
            let label:UILabel = UILabel()
            label.frame = CGRect(x: 0, y: offSet, width: scrollView.frame.width, height: 30)
            label.layer.borderWidth = 0.25
            label.layer.borderColor = UIColor.grayColor().CGColor
            
            if array == levelArray{
                label.tag = index+12
            }
            else if array == provinceArray{
                label.tag = index+40
            }
            else if array == districtArray{
                label.tag = index+50
            }
            else {
                label.tag = index + 90
            }
            
            label.backgroundColor = UIColor.whiteColor()
            label.text = array[index]
            label.textAlignment = NSTextAlignment.Center
            label.userInteractionEnabled = true
            
            let labelTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleLabelTap:"))
            label.addGestureRecognizer(labelTap)
            
            scrollView.addSubview(label)
            offSet += 30
        }
    }
    
    func handleLabelTap(sender:UITapGestureRecognizer){
        var tochedTag:Int = sender.view!.tag
        var textFromLabel:UILabel = UILabel()
        if tochedTag >= 12 {
            textFromLabel = self.view.viewWithTag(tochedTag) as! UILabel
        }
        
        let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
        textfield.text = textFromLabel.text
        
        closeDropDownPicker(levelDropDownView, textfield: textfield)
        closeDropDownPicker(provinceDropDownView, textfield: textfield)
        closeDropDownPicker(districtDropDownView, textfield: textfield)
        closeDropDownPicker(sectorDropDownView, textfield: textfield)
    }
    
    func openDropDownPicker(dropDownView: UIView, textfield:UITextField)
    {
        dropDownView.hidden = false
        
        if ((pickerHeight + 73) > (self.view.frame.height - (textfield.frame.origin.y + textfield.frame.height))){
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y - self.pickerHeight + 65), width: textfield.frame.width, height: self.pickerHeight)
                    dropDownView.alpha = 1
            })
        }
        else{
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y + textfield.frame.height + 65), width: textfield.frame.width, height: self.pickerHeight)
                    dropDownView.alpha = 1
            })
        }
    }
    
    func closeDropDownPicker(dropDownView: UIView, textfield:UITextField)
    {
        if ((pickerHeight + 73) > (self.view.frame.height - (textfield.frame.origin.y + textfield.frame.height))){
            UIView.animateWithDuration(0.6,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y - self.pickerHeight + 65) + 10, width: textfield.frame.width, height: self.pickerHeight)
                    dropDownView.alpha = 0
                },
                completion: { finished in
                    dropDownView.hidden = true
                }
            )
        }
        else{
            UIView.animateWithDuration(0.6,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y + textfield.frame.height) - 10 , width: textfield.frame.width, height: dropDownView.frame.height)
                    dropDownView.alpha = 0
                },
                completion: { finished in
                    dropDownView.hidden = true
                }
            )
        }
    }
    
    // =================================================================
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        closePicker()
        closeDropDownPicker(levelDropDownView, textfield: chooseLevelTextField)
        closeDropDownPicker(provinceDropDownView, textfield: provinceTextField)
        closeDropDownPicker(districtDropDownView, textfield: districtTextField)
        closeDropDownPicker(sectorDropDownView, textfield: sectorTextField)
        
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToNYCWelcomePage"  {
            
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! welcomeLeaderViewController
            toViewController.iphoneType = self.iphoneType!
            toViewController.transitioningDelegate = self.transitionManager
            
            // transfer user data
            toViewController.user_email = self.user_email!
            toViewController.user_password = self.user_password!
            toViewController.user_gender = self.user_gender!
            toViewController.user_status = self.user_status!
            toViewController.user_first_name = self.user_first_name!
            toViewController.user_last_name = self.user_last_name!
            toViewController.user_affiliation = self.user_affiliation!
            toViewController.user_phone_number = self.user_phone_number!
            toViewController.user_group = self.user_group!
            toViewController.user_subgroup = self.user_subgroup!
            toViewController.user_province = self.user_province!
            toViewController.user_district = self.user_district!
            toViewController.user_sector = self.user_sector!
            
        }
    }
    
    // unwind the segue
    @IBAction func handleBackButtonClick(sender:UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}