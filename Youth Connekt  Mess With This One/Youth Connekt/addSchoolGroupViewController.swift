//
//  addSchoolGroupViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 6/7/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class addSchoolGroupViewController: UIViewController,UITextFieldDelegate {
    // -------------- user data-----------------
    var user_name:String?
    var user_first_name:String?
    var user_last_name:String?
    var user_gender:String?
    var user_status:String?
    var user_image:UIImage?
    var user_phone_number:String?
    var user_email:String?
    var user_password:String?
    var user_group:String?
    var user_subgroup:String?
    var user_affiliation:String?
    var user_province:String?
    var user_district:String?
    var user_sector:String?
    // -------------- user data-----------------
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    
    @IBOutlet weak var mainNavBar: UINavigationBar!
    var containerView:UIView = UIView()
    
    // Screen Properties
    var spacingViews:CGFloat = 4
    let sideSpace:CGFloat = 40
    
    // Outlets
    var provinceTextField:UITextField = UITextField()
    var provinceDropDownButton:UIButton = UIButton()
    
    var districtTextField:UITextField = UITextField()
    var districtDropDownButton:UIButton = UIButton()
    
    var sectorTextField:UITextField = UITextField()
    var sectorDropDownButton:UIButton = UIButton()
    
    var addMyGroupButton:UIButton = UIButton()
    
    // type of iphone
    var iphoneType:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        createViews(5, containerViewHeight: (self.view.frame.height - (mainNavBar.frame.origin.y + mainNavBar.frame.height)),containerViewWidth: self.view.frame.width)
        createPicker()
    }
    
    func createViews(typeOfIphone:Int, containerViewHeight:CGFloat, containerViewWidth:CGFloat)
    {
        var youthConnektLogoSize:CGFloat = 0
        var spaceLogo_NavBar:CGFloat = 0
        var middlePartOffset:CGFloat = 0
        
        switch typeOfIphone{
        case 4:
            spacingViews = 20
            break
        case 5:
            spacingViews = 30
            break
        case 6:
            spacingViews = 50
            break
        case 61:
            spacingViews = 60
            
            break
        default:
            println("choose Iphone!")
        }
        
        containerView.frame = CGRect(x: 0, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height), width: containerViewWidth, height: containerViewHeight)
        containerView.backgroundColor = UIColor(red: (232/255), green: (227/255), blue: (216/255), alpha: 1)
        
        //============================ provinceTextField ===================================
        provinceTextField.frame = CGRect(x: sideSpace, y: 60 + (spacingViews*2), width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        provinceTextField.userInteractionEnabled = false
        provinceTextField.delegate = self
        provinceTextField.tag = 2
        provinceTextField.placeholder = "province"
        provinceTextField.textColor = UIColor.blueColor()
        
        var provinceBorder = CALayer()
        var width = CGFloat(0.75)
        provinceBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        provinceBorder.borderWidth = width
        provinceBorder.frame = CGRectMake(0, provinceTextField.frame.size.height - width, provinceTextField.frame.size.width, provinceTextField.frame.size.height)
        provinceTextField.layer.addSublayer(provinceBorder)
        provinceTextField.layer.masksToBounds = true
        
        containerView.addSubview(provinceTextField)
        
        provinceDropDownButton.frame = CGRect(x: (provinceTextField.frame.origin.x + provinceTextField.frame.width - 2), y: provinceTextField.frame.origin.y, width: 22, height: 30)
        provinceDropDownButton.tag = 3
        provinceDropDownButton.layer.cornerRadius = 3
        provinceDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        var groupDropDownBorder = CALayer()
        groupDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        groupDropDownBorder.borderWidth = width
        groupDropDownBorder.frame = CGRectMake(0, provinceDropDownButton.frame.size.height - width, provinceDropDownButton.frame.size.width, provinceDropDownButton.frame.size.height)
        provinceDropDownButton.layer.addSublayer(groupDropDownBorder)
        provinceDropDownButton.layer.masksToBounds = true
        provinceDropDownButton.addTarget(self, action: "dropDownTheList:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(provinceDropDownButton)
        //============================ provinceTextField ===================================
        
        //============================ districtTextField ===================================
        districtTextField.frame = CGRect(x: sideSpace, y: (provinceTextField.frame.origin.y + provinceTextField.frame.height + spacingViews), width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        districtTextField.userInteractionEnabled = false
        districtTextField.delegate = self
        districtTextField.tag = 4
        districtTextField.placeholder = "district"
        districtTextField.textColor = UIColor.blueColor()
        
        var districtBorder = CALayer()
        districtBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        districtBorder.borderWidth = width
        districtBorder.frame = CGRectMake(0, districtTextField.frame.size.height - width, districtTextField.frame.size.width, districtTextField.frame.size.height)
        districtTextField.layer.addSublayer(districtBorder)
        districtTextField.layer.masksToBounds = true
        
        containerView.addSubview(districtTextField)
        
        districtDropDownButton.frame = CGRect(x: (districtTextField.frame.origin.x + districtTextField.frame.width - 2), y: districtTextField.frame.origin.y, width: 22, height: 30)
        districtDropDownButton.tag = 5
        districtDropDownButton.layer.cornerRadius = 3
        districtDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        var disrictDropDownBorder = CALayer()
        disrictDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        disrictDropDownBorder.borderWidth = width
        disrictDropDownBorder.frame = CGRectMake(0, districtDropDownButton.frame.size.height - width, districtDropDownButton.frame.size.width, provinceDropDownButton.frame.size.height)
        districtDropDownButton.layer.addSublayer(disrictDropDownBorder)
        districtDropDownButton.layer.masksToBounds = true
        districtDropDownButton.addTarget(self, action: "dropDownTheList:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(districtDropDownButton)
        //============================ districtTextField ===================================
        
        //============================ sectorTextField ===================================
        sectorTextField.frame = CGRect(x: sideSpace, y: (districtTextField.frame.origin.y + districtTextField.frame.height + spacingViews) , width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        sectorTextField.userInteractionEnabled = false
        sectorTextField.delegate = self
        sectorTextField.tag = 6
        sectorTextField.placeholder = "sector"
        sectorTextField.textColor = UIColor.blueColor()
        
        var sectorBorder = CALayer()
        sectorBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        sectorBorder.borderWidth = width
        sectorBorder.frame = CGRectMake(0, sectorTextField.frame.size.height - width, sectorTextField.frame.size.width, sectorTextField.frame.size.height)
        sectorTextField.layer.addSublayer(sectorBorder)
        sectorTextField.layer.masksToBounds = true
        
        containerView.addSubview(sectorTextField)
        
        sectorDropDownButton.frame = CGRect(x: (sectorTextField.frame.origin.x + sectorTextField.frame.width - 2), y: sectorTextField.frame.origin.y, width: 22, height: 30)
        sectorDropDownButton.tag = 7
        sectorDropDownButton.layer.cornerRadius = 3
        sectorDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        var sectorDropDownBorder = CALayer()
        sectorDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        sectorDropDownBorder.borderWidth = width
        sectorDropDownBorder.frame = CGRectMake(0, sectorDropDownButton.frame.size.height - width, sectorDropDownButton.frame.size.width, sectorDropDownButton.frame.size.height)
        sectorDropDownButton.layer.addSublayer(sectorDropDownBorder)
        sectorDropDownButton.layer.masksToBounds = true
        sectorDropDownButton.addTarget(self, action: "dropDownTheList:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(sectorDropDownButton)
        //============================ sectorTextField ===================================
        addMyGroupButton.frame = CGRect(x: (containerView.frame.width/2 - 60), y: (containerView.frame.height - 100), width: 120, height: 30)
        addMyGroupButton.tag = 8
        addMyGroupButton.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        addMyGroupButton.layer.borderWidth = 1
        addMyGroupButton.layer.cornerRadius = 3
        addMyGroupButton.setTitleColor(UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1), forState: .Normal)
        addMyGroupButton.setTitle("Add Group", forState: .Normal)
        addMyGroupButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        addMyGroupButton.titleLabel?.textAlignment = NSTextAlignment.Center
        addMyGroupButton.addTarget(self, action: "addGroupButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(addMyGroupButton)
        
        println("Done with container view")
    }
    
    // button click handlers
    func addGroupButtonClick(sender:UIButton){
        closePicker()
        if provinceTextField.text.isEmpty||districtTextField.text.isEmpty||sectorTextField.text.isEmpty{
            showAlertController("Please fill all required information")
        }
        else{
            performSegueWithIdentifier("goToSchoolWelcomePage", sender: self)
        }
    }
    
    func showAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "About"],
            ["title" : "Settings"],
            ["title" : "Share"],
            ["title" : "Sign In"]
        ]
    }
    
    @IBAction func pickerSelect(sender: UIBarButtonItem)
    {
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createPicker()
    {
        pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        pickerMenu.layer.borderWidth = 0.75
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func openPicker()
    {
        closeDropDownPicker(provinceDropDownView, textfield: provinceTextField)
        closeDropDownPicker(districtDropDownView, textfield: districtTextField)
        closeDropDownPicker(sectorDropDownView, textfield: sectorTextField)
        
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    func buttonClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/360), green: (47/360), blue: (61/360), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        switch sender.tag{
        case 0: // About
            performSegueWithIdentifier("fromaddGroupSchoolToAbout", sender: self)
            break
        case 1: // Settings
            performSegueWithIdentifier("fromaddGroupSchoolToSettings", sender: self)
            break
        case 2: // Share
            performSegueWithIdentifier("fromaddGroupSchoolToShare", sender: self)
            break
        case 3: // Sign In
            
            break
        default:
            println("No such segue exists in your app!")
        }
        
        closePicker()
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    
    //------------------------------------------------------------------------------------------
    // Drop Down Section
    //------------------------------------------------------------------------------------------
    let provinceDropDownView = UIView(frame: CGRectZero)
    let districtDropDownView = UIView(frame: CGRectZero)
    let sectorDropDownView = UIView(frame: CGRectZero)
    
    var scrollView = UIScrollView()
    var textField = UITextField()
    
    // textfield tags
    var textfieldTag:Int = 0
    var dropDownTag:Int = 0
    
    // random info
    let provinceArray:Array<String> = ["Northern province","Southern province","Kigali city","Eastern province","Western province"]
    let districtArray:Array<String> = ["Gasabo","Nyarugenge","Kicukiro"]
    let sectorArray:Array<String> = ["Gatsata","Kinyinya","Jabana"]
    
    func dropDownTheList(sender: UIButton)
    {
        closePicker()
        switch sender{
        case provinceDropDownButton:
            textfieldTag = 2
            dropDownTag = 21
            
            let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
            createDropDownList(provinceDropDownView, textfield: textfield, array: provinceArray)
            openDropDownPicker(provinceDropDownView, textfield: textfield)
            
            break
        case districtDropDownButton:
            textfieldTag = 4
            dropDownTag = 22
            
            let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
            createDropDownList(districtDropDownView, textfield: textfield, array: districtArray)
            openDropDownPicker(districtDropDownView, textfield: textfield)
            
            break
        case sectorDropDownButton:
            textfieldTag = 6
            dropDownTag = 23
            
            let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
            createDropDownList(sectorDropDownView, textfield: textfield, array: sectorArray)
            openDropDownPicker(sectorDropDownView, textfield: textfield)
            
            break
        default:
            println("In the drop down select!")
        }
    }
    
    var pickerHeight:CGFloat = 0
    func createDropDownList(dropDownView: UIView, textfield:UITextField, array:[String])
    {
        var contentSize:CGFloat = (CGFloat)(array.count*30)
        if array.count*30 > 150 {
            pickerHeight = 150
        }
        else{
            pickerHeight = (CGFloat)(array.count*30)
        }
        
        if ((pickerHeight + 73) > (self.view.frame.height - (textfield.frame.origin.y + textfield.frame.height))){
            dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y - pickerHeight) + 10, width: textfield.frame.width, height: pickerHeight)
            
            if (dropDownView == self.provinceDropDownView){
                dropDownView.tag = 21
            }
            else if (dropDownView == self.districtDropDownView){
                dropDownView.tag = 22
            }
            else{
                dropDownView.tag = 23
            }
            
            dropDownView.layer.cornerRadius = 3
            dropDownView.alpha = 0
            dropDownView.hidden = true
            dropDownView.backgroundColor = UIColor.whiteColor()
            dropDownView.layer.borderWidth = 0.5
            dropDownView.layer.borderColor = UIColor.blackColor().CGColor
            dropDownView.userInteractionEnabled = true
            view.addSubview(dropDownView)
            
        }
        else{
            
            // create the picker drop down view
            dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y + textfield.frame.height) - 10 , width: textfield.frame.width, height: pickerHeight)
            dropDownView.tag = 20
            
            if (dropDownView == self.provinceDropDownView){
                dropDownView.tag = 21
            }
            else if (dropDownView == self.districtDropDownView){
                dropDownView.tag = 22
            }
            else{
                dropDownView.tag = 23
            }
            
            dropDownView.layer.cornerRadius = 3
            dropDownView.alpha = 0
            dropDownView.hidden = true
            dropDownView.backgroundColor = UIColor.whiteColor()
            dropDownView.layer.borderWidth = 0.5
            dropDownView.layer.borderColor = UIColor.blackColor().CGColor
            dropDownView.userInteractionEnabled = true
            view.addSubview(dropDownView)
        }
        
        // Add a scroll view to it
        scrollView.frame = CGRect(x: 0, y: 0, width: dropDownView.bounds.width, height: pickerHeight)
        //scrollView.layer.cornerRadius = 3
        scrollView.backgroundColor = UIColor.whiteColor()
        scrollView.contentSize = CGSize(width: dropDownView.bounds.width, height: contentSize)
        dropDownView.addSubview(scrollView)
        
        // Add labels for the user selection
        var offSet:CGFloat = 0
        
        for( var index = 0; index < array.count; index++){
            let label:UILabel = UILabel()
            label.frame = CGRect(x: 0, y: offSet, width: scrollView.frame.width, height: 30)
            label.layer.borderWidth = 0.25
            label.layer.borderColor = UIColor.grayColor().CGColor
            if array == provinceArray{
                label.tag = index+12
            }
            else if array == districtArray{
                label.tag = index+40
            }
            else{
                label.tag = index+80
            }
            label.backgroundColor = UIColor.whiteColor()
            label.text = array[index]
            label.textAlignment = NSTextAlignment.Center
            label.userInteractionEnabled = true
            
            let labelTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleLabelTap:"))
            label.addGestureRecognizer(labelTap)
            
            scrollView.addSubview(label)
            offSet += 30
        }
    }
    
    func handleLabelTap(sender:UITapGestureRecognizer){
        var tochedTag:Int = sender.view!.tag
        var textFromLabel:UILabel = UILabel()
        if tochedTag >= 12 {
            textFromLabel = self.view.viewWithTag(tochedTag) as! UILabel
        }
        else if tochedTag >= 40 {
            textFromLabel = self.view.viewWithTag(tochedTag) as! UILabel
        }
        
        let textfield:UITextField = self.view.viewWithTag(textfieldTag) as! UITextField
        textfield.text = textFromLabel.text
        
        closeDropDownPicker(provinceDropDownView, textfield: textfield)
        closeDropDownPicker(districtDropDownView, textfield: textfield)
        closeDropDownPicker(sectorDropDownView, textfield: textfield)
    }
    
    func openDropDownPicker(dropDownView: UIView, textfield:UITextField)
    {
        dropDownView.hidden = false
        
        if ((pickerHeight + 73) > (self.view.frame.height - (textfield.frame.origin.y + textfield.frame.height))){
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y - self.pickerHeight + 65), width: textfield.frame.width, height: self.pickerHeight)
                    dropDownView.alpha = 1
            })
        }
        else{
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y + textfield.frame.height + 65), width: textfield.frame.width, height: self.pickerHeight)
                    dropDownView.alpha = 1
            })
        }
    }
    
    func closeDropDownPicker(dropDownView: UIView, textfield:UITextField)
    {
        if ((pickerHeight + 73) > (self.view.frame.height - (textfield.frame.origin.y + textfield.frame.height))){
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y - self.pickerHeight + 65) + 10, width: textfield.frame.width, height: self.pickerHeight)
                    dropDownView.alpha = 0
                },
                completion: { finished in
                    dropDownView.hidden = true
                }
            )
        }
        else{
            UIView.animateWithDuration(0.3,
                animations: {
                    dropDownView.frame = CGRect(x: textfield.frame.origin.x, y: (textfield.frame.origin.y + textfield.frame.height) - 10 , width: textfield.frame.width, height: dropDownView.frame.height)
                    dropDownView.alpha = 0
                },
                completion: { finished in
                    dropDownView.hidden = true
                }
            )
        }
    }
    
    // =================================================================
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        closePicker()
        closeDropDownPicker(provinceDropDownView, textfield: provinceTextField)
        closeDropDownPicker(districtDropDownView, textfield: districtTextField)
        closeDropDownPicker(sectorDropDownView, textfield: sectorTextField)
        
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToSchoolWelcomePage"  {
            
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! welcomeLeaderViewController
            toViewController.iphoneType = self.iphoneType!
            toViewController.transitioningDelegate = self.transitionManager
        }
    }
    
    // unwind the segue
    @IBAction func handleBackButtonClick(sender:UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}