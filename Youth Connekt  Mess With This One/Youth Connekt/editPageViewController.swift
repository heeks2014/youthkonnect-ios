//
//  editPageViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 6/3/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit
import MobileCoreServices

class editPageViewController: UIViewController, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    
    // -------------- user data-----------------
    // -------------- user data-----------------
    var user_name:String?
    var user_first_name:String?
    var user_last_name:String?
    var user_gender:String?
    var user_status:String?
    var user_image:UIImage?
    var user_phone_number:String?
    var user_email:String?
    var user_password:String?
    var user_group:String?
    var user_subgroup:String?
    var user_affiliation:String?
    var user_province:String?
    var user_district:String?
    var user_sector:String?
    // -------------- user data-----------------
    // -------------- user data-----------------
    @IBOutlet weak var mainNavBar: UINavigationBar!
    
    // Screen Properties
    var spacingViews:CGFloat = 4
    let sideSpace:CGFloat = 10
    
    // profile Image
    var profileImage:UIImageView = UIImageView()
    
    // image chooser
    var imageChooserButton:UIButton = UIButton()
    var takePicture:UIButton = UIButton()
    
    // user status
    // Label
    var changeUserStatusLabel:UILabel = UILabel()
    // text box
    var changeUserStatus:UITextField = UITextField()
    
    // drop down button
    var dropDownButton:UIButton = UIButton()
    
    // user name
    // label
    // user name label
    var userNameLabel:UILabel = UILabel()
    // text box
    var userFirstName:UITextField = UITextField()
    var userLastName:UITextField = UITextField()
    
    // user phone number
    // Label
    var userPhoneNumberLabel:UILabel = UILabel()
    // text box
    var userPhoneNumber:UITextField = UITextField()
    
    // user email
    // Label
    var userEmailLabel:UILabel = UILabel()
    // Textfield
    var userEmail:UITextField = UITextField()
    
    // user group
    // Label
    var userGroupLabel:UILabel = UILabel()
    // Textfield
    var userGroup:UITextField = UITextField()
    
    // user Affiliation
    // Label
    var userAffiliationLabel:UILabel = UILabel()
    // Textfield
    var userAffiliation:UITextField = UITextField()
    
    // user Province
    // Label
    var userProvinceLabel:UILabel = UILabel()
    // Textfield
    var userProvince:UITextField = UITextField()
    
    // user District
    // Label
    var userDistrictLabel:UILabel = UILabel()
    // Textfield
    var userDistrict:UITextField = UITextField()
    
    // user sector
    // Label
    var userSectorLabel:UILabel = UILabel()
    // Textfield
    var userSector:UITextField = UITextField()
    
    
    // type of iphone
    var iphoneType:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: (232/255), green: (227/255), blue: (216/255), alpha: 0.9)
        
        createAllViewFields(iphoneType!, containerViewHeight: self.view.frame.height,containerViewWidth: self.view.frame.width)
        
        createDropDownStatus()
        
        userFirstName.delegate = self
        userLastName.delegate = self
        userPhoneNumber.delegate = self
        userEmail.delegate = self
        userGroup.delegate = self
        userDistrict.delegate = self
        userAffiliation.delegate = self
        userSector.delegate = self
        userProvince.delegate = self
    }
    
    func createAllViewFields(typeOfIphone:Int, containerViewHeight:CGFloat, containerViewWidth:CGFloat)
    {
        var profileImageSize:CGFloat = 0
        var spaceImageChooser_StatusDropDwn:CGFloat = 0
        var middlePartOffset:CGFloat = 0
        
        switch typeOfIphone{
        case 4:
            profileImageSize = 120
            spaceImageChooser_StatusDropDwn = 35
            middlePartOffset = 10
            spacingViews = 4
            break
        case 5:
            profileImageSize = 150
            spaceImageChooser_StatusDropDwn = 50
            middlePartOffset = 20
            spacingViews = 8
            break
        case 6:
            profileImageSize = 200
            spaceImageChooser_StatusDropDwn = 50
            middlePartOffset = 20
            spacingViews = 12
            break
        case 61:
            profileImageSize = 200
            spaceImageChooser_StatusDropDwn = 50
            middlePartOffset = 20
            spacingViews = 15
            break
        default:
            println("choose Iphone!")
        }
        
        var xPosition:CGFloat = sideSpace
        var yPosition:CGFloat = 0
        
        // TOP PART
        // Profile Image
        profileImage.frame = CGRect(x: xPosition, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height) + spacingViews , width: profileImageSize, height: profileImageSize)
        profileImage.image = self.user_image!
        profileImage.layer.borderColor = UIColor.whiteColor().CGColor
        profileImage.layer.borderWidth = 0.5
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.clipsToBounds = true
        self.view.addSubview(profileImage)
        
        // edit button
        imageChooserButton.frame = CGRect(x: (containerViewWidth - (sideSpace + 120)), y: profileImage.frame.origin.y , width: 80, height: 30)
        imageChooserButton.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        imageChooserButton.layer.cornerRadius = 3
        imageChooserButton.layer.borderColor = UIColor.whiteColor().CGColor
        imageChooserButton.layer.borderWidth = 0.5
        imageChooserButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        imageChooserButton.setTitle("Image", forState: .Normal)
        imageChooserButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        imageChooserButton.titleLabel?.textAlignment = NSTextAlignment.Left
        imageChooserButton.addTarget(self, action: "handleImageChooserBtnClick:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(imageChooserButton)
        
        takePicture.frame = CGRect(x: (imageChooserButton.frame.origin.x + imageChooserButton.frame.width + 2), y: imageChooserButton.frame.origin.y , width: 38, height: 30)
        takePicture.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        takePicture.layer.cornerRadius = 3
        takePicture.layer.borderColor = UIColor.whiteColor().CGColor
        takePicture.layer.borderWidth = 0.5
        takePicture.setImage(UIImage(named: "SLR Camera-50"), forState: UIControlState.Normal)
        takePicture.addTarget(self, action: "handleTakePictureBtn:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(takePicture)
        
        // user status
        changeUserStatus.frame = CGRect(x: (containerViewWidth - (sideSpace + 120)), y: imageChooserButton.frame.origin.y + spaceImageChooser_StatusDropDwn, width: 100, height: 30)
        changeUserStatus.backgroundColor = UIColor.whiteColor()
        changeUserStatus.textAlignment = NSTextAlignment.Center
        changeUserStatus.layer.cornerRadius = 2
        changeUserStatus.placeholder = "Status"
        changeUserStatus.text = self.user_status!
        changeUserStatus.textColor = UIColor.blueColor()
        self.view.addSubview(changeUserStatus)
        
        // drop down button
        dropDownButton.frame = CGRect(x: (containerViewWidth - (sideSpace + 120 - 100 + 2)), y: imageChooserButton.frame.origin.y + spaceImageChooser_StatusDropDwn, width: 22, height: 30)
        dropDownButton.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        dropDownButton.layer.cornerRadius = 2
        dropDownButton.setBackgroundImage(UIImage(named: "dropDown-50"), forState: UIControlState.Normal)
        
        dropDownButton.addTarget(self, action: "pickStatus:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(dropDownButton)
        
        //MIDDLE PART
        yPosition += middlePartOffset + (profileImage.frame.origin.y + profileImage.frame.size.height)
        
        // Label
        userNameLabel.frame = CGRect(x: sideSpace , y: yPosition, width: 105, height: 30)
        userNameLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userNameLabel.textAlignment = NSTextAlignment.Left
        userNameLabel.text = " NAME"
        userNameLabel.textColor = UIColor.whiteColor()
        self.view.addSubview(userNameLabel)
        // TextField
        userFirstName.frame = CGRect(x: (userNameLabel.frame.width+10), y: yPosition, width: (containerViewWidth-(105+20)-2) / 2 , height: 30)
        userFirstName.tag = 1
        userFirstName.backgroundColor = UIColor.whiteColor()
        userFirstName.textAlignment = NSTextAlignment.Center
        userFirstName.textColor = UIColor.blueColor()
        userFirstName.placeholder = "first name"
        userFirstName.text = self.user_first_name!
        self.view.addSubview(userFirstName)
        
        // TextField
        userLastName.frame = CGRect(x: (userFirstName.frame.origin.x + userFirstName.frame.width + 2), y: userFirstName.frame.origin.y, width: userFirstName.frame.width, height: 30)
        userLastName.tag = 1
        userLastName.backgroundColor = UIColor.whiteColor()
        userLastName.textAlignment = NSTextAlignment.Center
        userLastName.textColor = UIColor.blueColor()
        userLastName.placeholder = "last name"
        userLastName.text = self.user_last_name!
        self.view.addSubview(userLastName)
        
        // Label
        yPosition += spacingViews + userNameLabel.frame.size.height
        userPhoneNumberLabel.frame = CGRect(x: sideSpace , y: yPosition, width: 105, height: 30)
        userPhoneNumberLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userPhoneNumberLabel.textAlignment = NSTextAlignment.Left
        userPhoneNumberLabel.text = " PHONE"
        userPhoneNumberLabel.textColor = UIColor.whiteColor()
        self.view.addSubview(userPhoneNumberLabel)
        // TextField
        userPhoneNumber.frame = CGRect(x: (userPhoneNumberLabel.frame.width+10), y: yPosition, width: (containerViewWidth-(105+20)), height: 30)
        userPhoneNumber.tag = 2
        userPhoneNumber.backgroundColor = UIColor.whiteColor()
        userPhoneNumber.textAlignment = NSTextAlignment.Center
        userPhoneNumber.textColor = UIColor.blueColor()
        userPhoneNumber.placeholder = "phone number"
        userPhoneNumber.text = self.user_phone_number!
        self.view.addSubview(userPhoneNumber)
        
        // Label
        yPosition += spacingViews + userPhoneNumberLabel.frame.size.height
        userEmailLabel.frame = CGRect(x: sideSpace , y: yPosition, width: 105, height: 30)
        userEmailLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userEmailLabel.textAlignment = NSTextAlignment.Left
        userEmailLabel.text = " EMAIL"
        userEmailLabel.textColor = UIColor.whiteColor()
        self.view.addSubview(userEmailLabel)
        // TextField
        userEmail.frame = CGRect(x: (userPhoneNumberLabel.frame.width+10), y: yPosition, width: (containerViewWidth-(105+20)), height: 30)
        userEmail.tag = 3
        userEmail.backgroundColor = UIColor.whiteColor()
        userEmail.textAlignment = NSTextAlignment.Center
        userEmail.textColor = UIColor.blueColor()
        userEmail.placeholder = "email"
        userEmail.text = self.user_email!
        self.view.addSubview(userEmail)
        
        // Label
        yPosition += spacingViews + userEmailLabel.frame.size.height
        userGroupLabel.frame = CGRect(x: sideSpace , y: yPosition, width: 105, height: 30)
        userGroupLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userGroupLabel.textAlignment = NSTextAlignment.Left
        userGroupLabel.text = " GROUP"
        userGroupLabel.textColor = UIColor.whiteColor()
        self.view.addSubview(userGroupLabel)
        // TextField
        userGroup.frame = CGRect(x: (userPhoneNumberLabel.frame.width+10), y: yPosition, width: (containerViewWidth-(105+20)), height: 30)
        userGroup.tag = 4
        userGroup.backgroundColor = UIColor.whiteColor()
        userGroup.textAlignment = NSTextAlignment.Center
        userGroup.textColor = UIColor.blueColor()
        userGroup.placeholder = "group"
        userGroup.text = self.user_group!
        self.view.addSubview(userGroup)
        
        // Label
        yPosition += spacingViews + userGroupLabel.frame.size.height
        userAffiliationLabel.frame = CGRect(x: sideSpace , y: yPosition, width: 105, height: 30)
        userAffiliationLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userAffiliationLabel.textAlignment = NSTextAlignment.Left
        userAffiliationLabel.text = " AFFILIATION"
        userAffiliationLabel.textColor = UIColor.whiteColor()
        self.view.addSubview(userAffiliationLabel)
        // TextField
        userAffiliation.frame = CGRect(x: (userPhoneNumberLabel.frame.width+10), y: yPosition, width: (containerViewWidth-(105+20)), height: 30)
        userAffiliation.tag = 5
        userAffiliation.backgroundColor = UIColor.whiteColor()
        userAffiliation.textAlignment = NSTextAlignment.Center
        userAffiliation.textColor = UIColor.blueColor()
        userAffiliation.placeholder = "affiliation"
        userAffiliation.text = self.user_affiliation!
        self.view.addSubview(userAffiliation)
        
        // Label
        yPosition += spacingViews + userAffiliationLabel.frame.size.height
        userProvinceLabel.frame = CGRect(x: sideSpace , y: yPosition, width: 105, height: 30)
        userProvinceLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userProvinceLabel.textAlignment = NSTextAlignment.Left
        userProvinceLabel.text = " PROVINCE"
        userProvinceLabel.textColor = UIColor.whiteColor()
        self.view.addSubview(userProvinceLabel)
        // TextField
        userProvince.frame = CGRect(x: (userPhoneNumberLabel.frame.width+10), y: yPosition, width: (containerViewWidth-(105+20)), height: 30)
        userProvince.tag = 6
        userProvince.backgroundColor = UIColor.whiteColor()
        userProvince.textAlignment = NSTextAlignment.Center
        userProvince.textColor = UIColor.blueColor()
        userProvince.placeholder = "province"
        userProvince.text = self.user_province!
        self.view.addSubview(userProvince)
        
        // Label
        yPosition += spacingViews + userProvinceLabel.frame.size.height
        userDistrictLabel.frame = CGRect(x: sideSpace , y: yPosition, width: 105, height: 30)
        userDistrictLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userDistrictLabel.textAlignment = NSTextAlignment.Left
        userDistrictLabel.text = " DISTRICT"
        userDistrictLabel.textColor = UIColor.whiteColor()
        self.view.addSubview(userDistrictLabel)
        // TextField
        userDistrict.frame = CGRect(x: (userPhoneNumberLabel.frame.width+10), y: yPosition, width: (containerViewWidth-(105+20)), height: 30)
        userDistrict.tag = 7
        userDistrict.backgroundColor = UIColor.whiteColor()
        userDistrict.textAlignment = NSTextAlignment.Center
        userDistrict.textColor = UIColor.blueColor()
        userDistrict.placeholder = "district"
        userDistrict.text = self.user_district!
        self.view.addSubview(userDistrict)
        
        // Label
        yPosition += spacingViews + userDistrictLabel.frame.size.height
        userSectorLabel.frame = CGRect(x: sideSpace , y: yPosition, width: 105, height: 30)
        userSectorLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userSectorLabel.textAlignment = NSTextAlignment.Left
        userSectorLabel.text = " SECTOR"
        userSectorLabel.textColor = UIColor.whiteColor()
        self.view.addSubview(userSectorLabel)
        // TextField
        userSector.frame = CGRect(x: (userPhoneNumberLabel.frame.width+10), y: yPosition, width: (containerViewWidth-(105+20)), height: 30)
        userSector.tag = 8
        userSector.backgroundColor = UIColor.whiteColor()
        userSector.textAlignment = NSTextAlignment.Center
        userSector.textColor = UIColor.blueColor()
        userSector.placeholder = "sectror"
        userSector.text = self.user_sector!
        self.view.addSubview(userSector)
        
        
    }
    
    //------------------------------------------------------------------------------------------
    // Menu Section
    //------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "Leader"],
            ["title" : "Member"],
        ]
    }
    
    func pickStatus(sender: UIButton)
    {
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createDropDownStatus()
    {
        pickerMenu.frame = CGRect(x: (self.changeUserStatus.frame.origin.x), y: (self.changeUserStatus.frame.origin.y + 31), width: 100, height: 60)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        pickerMenu.layer.borderWidth = 0.75
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func buttonClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/360), green: (47/360), blue: (61/360), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        self.changeUserStatus.text = sender.titleLabel?.text
        closePicker()
    }
    
    func openPicker()
    {
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.changeUserStatus.frame.origin.x), y: (self.changeUserStatus.frame.origin.y + 31), width: 100, height: 60)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.changeUserStatus.frame.origin.x), y: (self.changeUserStatus.frame.origin.y + 31), width: 100, height: 63)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    //------------------------------------------------------------------------------------------
    // Menu Section
    //------------------------------------------------------------------------------------------
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    var kbHeight: CGFloat!
    var textFieldTag: Int = -1
    
    func animateTextField(up: Bool) {
        var movement = (up ? -kbHeight : kbHeight)
        
        UIView.animateWithDuration(0.3, animations: {
            self.view.frame = CGRectOffset(self.view.frame, 0, movement)
        })
    }
    
    override func viewWillAppear(animated:Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                let textfield:UITextField = self.view.viewWithTag(textFieldTag) as! UITextField
                if((keyboardSize.height + 25) > (self.view.frame.height - textfield.frame.origin.y)){
                    kbHeight = (keyboardSize.height - (self.view.frame.height - textfield.frame.origin.y)) + 35
                }
                else{
                    kbHeight = 0
                }
                self.animateTextField(true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.animateTextField(false)
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    
    // =================== Handle Save Button =======================
    func showAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func handleSaveButtonClick(sender:UIButton){
        if(changeUserStatus.text.isEmpty){
            showAlertController("please add your status!")
        }
        else{
            performSegueWithIdentifier("segueBackToUserPage", sender: self)
        }
    }
    // =================== Handle Save Button =======================
    
    //======================Image picker===========================
    var imagePicker:UIImagePickerController = UIImagePickerController()
    var newMedia: Bool?
    
    func handleImageChooserBtnClick(sender:UIButton){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum){
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
            imagePicker.allowsEditing = false
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
//        self.dismissViewControllerAnimated(true, completion: {()->Void in })
//        profileImage.image = image
//    }
    
    func handleTakePictureBtn(sender:UIButton){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            imagePicker.mediaTypes = [kUTTypeImage as NSString]
            imagePicker.allowsEditing = false
            
            self.presentViewController(imagePicker, animated: true, completion: nil)
            newMedia = true
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        self.dismissViewControllerAnimated(true, completion: nil)
        
        if mediaType.isEqualToString(kUTTypeImage as! String) {
            let image = info[UIImagePickerControllerOriginalImage]
                as! UIImage
            
            profileImage.image = image
            
            if (newMedia == true) {
                UIImageWriteToSavedPhotosAlbum(image, self,
                    "image:didFinishSavingWithError:contextInfo:", nil)
            }
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo:UnsafePointer<Void>) {
        
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                message: "Failed to save image",
                preferredStyle: UIAlertControllerStyle.Alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                style: .Cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true,
                completion: nil)
        }
    }
    
    //======================Image picker===========================
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if (!pickerMenu.hidden) {
            closePicker()
        }
        self.view.endEditing(true)
        
        textFieldTag = textField.tag
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == userEmail{
            if userEmail.text.isEmpty{
                showAlertController("Please enter your email")
            }
            else if !userEmail.text.isEmail{
                showAlertController("You entered Wrong Email!")
            }
        }
        else if textField == userPhoneNumber{
            if userPhoneNumber.text.isPhoneNumber{
                showAlertController("Please Enter the correct infos")
            }
            else if !userEmail.text.isEmpty{
                showAlertController("Please Enter the correct infos")
            }
        }
        else{
            if textField.text.isEmpty{
                showAlertController("Please Enter the correct infos")
            }
        }
    }
    
    // Handle the return button on a textfield
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    // Handle touches done outside the input fields
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        if (!pickerMenu.hidden) {
            closePicker()
        }
    }
    
    // unwind the segue
    @IBAction func handleCancelButtonClick(sender:UIButton){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveUserChanges(segue:UIStoryboardSegue) {
        
    }
    
    func saveUserData(){
        self.user_first_name = userFirstName.text
        self.user_last_name = userLastName.text
        self.user_status = changeUserStatus.text
        self.user_image = profileImage.image
        self.user_phone_number = userPhoneNumber.text
        self.user_email = userEmail.text
        self.user_group = userGroup.text
        self.user_affiliation = userAffiliation.text
        self.user_province = userProvince.text
        self.user_district = userDistrict.text
        self.user_sector = userSector.text
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        saveUserData()
        
//        if(userFirstName.text.isEmpty || userGroup.text.isEmpty || userPhoneNumber.text.isEmpty || userEmail.text.isEmpty || userAffiliation.text.isEmpty || userProvince.text.isEmpty || userDistrict.text.isEmpty || userSector.text.isEmpty){
//            showAlertController("please fill all the required information!")
//        }
        if segue.identifier == "segueBackToUserPage" {
            
            let toViewController = segue.destinationViewController as! userPageViewController
            
            toViewController.user_first_name = self.user_first_name
            toViewController.user_last_name = self.user_last_name
            toViewController.user_status = self.user_status
            toViewController.user_image = self.user_image
            toViewController.user_phone_number = self.user_phone_number
            toViewController.user_email = self.user_email
            toViewController.user_group = self.user_group
            toViewController.user_affiliation = self.user_affiliation
            toViewController.user_province = self.user_province
            toViewController.user_district = self.user_district
            toViewController.user_sector = self.user_sector
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}