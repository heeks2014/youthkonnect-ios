//
//  userPageViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 6/3/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class userPageViewController: UIViewController,UITextViewDelegate {
    // ========= Just for Testing ==============
    // -------------- user data-----------------
    var user_name:String?
    var user_first_name:String?
    var user_last_name:String?
    var user_gender:String?
    var user_status:String?
    var user_image:UIImage?
    var user_phone_number:String?
    var user_email:String?
    var user_password:String?
    var user_group:String?
    var user_subgroup:String?
    var user_affiliation:String?
    var user_province:String?
    var user_district:String?
    var user_sector:String?
    // -------------- user data-----------------
    // ========= Just for Testing ==============
    
    // type of iphone
    var iphoneType:Int?
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    
    @IBOutlet weak var mainNavBar: UINavigationBar!
    var allInOneView:UIView = UIView()
    
    // Screen Properties
    var spacingViews:CGFloat = 4
    let sideSpace:CGFloat = 10
    
    // profile Image
    var profileImage:UIImageView = UIImageView()
    // edit button
    var editButton:UIButton = UIButton()
    // user name label
    var profileName:UILabel = UILabel()
    // user status label
    var userStatus:UILabel = UILabel()
    
    // user phone number
    // Label
    var userPhoneNumberLabel:UILabel = UILabel()
    // text box
    var userPhoneNumber:UITextField = UITextField()
    
    // user email
    // Label
    var userEmailLabel:UILabel = UILabel()
    // Textfield
    var userEmail:UITextField = UITextField()
    
    // user group
    // Label
    var userGroupLabel:UILabel = UILabel()
    // Textfield
    var userGroup:UITextField = UITextField()
    
    // user Affiliation
    // Label
    var userAffiliationLabel:UILabel = UILabel()
    // Textfield
    var userAffiliation:UITextField = UITextField()
    
    // user Province
    // Label
    var userProvinceLabel:UILabel = UILabel()
    // Textfield
    var userProvince:UITextField = UITextField()
    
    // user District
    // Label
    var userDistrictLabel:UILabel = UILabel()
    // Textfield
    var userDistrict:UITextField = UITextField()
    
    // user sector
    // Label
    var userSectorLabel:UILabel = UILabel()
    // Textfield
    var userSector:UITextField = UITextField()
    
    // BOTTOM SECTION
    // container (White) view
    var bottomContainerView:UIView = UIView()
    // user messagebox
    var userMessageText:UITextView = UITextView()
    // send message button
    var sendMEssage:UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createAllViewFields(iphoneType!, mainContainerView: self.view, containerViewHeight: self.view.frame.height,containerViewWidth: self.view.frame.width)
        createPicker()
        
        if (iphoneType == 4){
            createIphone4Messagebutton()
            createIphone4MessageBox()
        }
        else{
            createButtomPart( iphoneType!, mainContainerView: self.view, containerViewWidth: self.view.frame.width , containerViewHeight: allInOneView.frame.height)
        }
    }
    
    func createAllViewFields(typeOfIphone:Int, mainContainerView:UIView, containerViewHeight:CGFloat, containerViewWidth:CGFloat)
    {
        var profileImageSize:CGFloat = 0
        var spaceImageChooser_StatusDropDwn:CGFloat = 0
        var middlePartOffset:CGFloat = 0
        var editButtonsSize:CGFloat = 0
        
        switch typeOfIphone{
        case 4:
            profileImageSize = 80
            spaceImageChooser_StatusDropDwn = 35
            middlePartOffset = 10
            editButtonsSize = 32
            spacingViews = 4
            break
        case 5:
            profileImageSize = 80
            spaceImageChooser_StatusDropDwn = 50
            middlePartOffset = 20
            editButtonsSize = 34
            spacingViews = 4
            break
        case 6:
            profileImageSize = 120
            spaceImageChooser_StatusDropDwn = 50
            middlePartOffset = 20
            editButtonsSize = 36
            spacingViews = 8
            break
        case 61:
            profileImageSize = 150
            spaceImageChooser_StatusDropDwn = 60
            middlePartOffset = 30
            editButtonsSize = 36
            spacingViews = 12
            break
        default:
            spacingViews = 10
        }
        
        
        var xPosition:CGFloat = sideSpace
        var yPosition:CGFloat = 0
        
        allInOneView.frame = CGRect(x: 0, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height), width: containerViewWidth, height: containerViewHeight - (mainNavBar.frame.origin.y + mainNavBar.frame.height))
        allInOneView.tag = 1
        allInOneView.backgroundColor = UIColor(red: (232/255), green: (227/255), blue: (216/255), alpha: 1)
        mainContainerView.addSubview(allInOneView)
        
        // TOP PART
        // Profile Image
        profileImage.frame = CGRect(x: ((allInOneView.frame.width/2) - (profileImageSize/2)), y: allInOneView.frame.origin.y + spacingViews , width: profileImageSize, height: profileImageSize)
        profileImage.tag = 2
        profileImage.image = user_image!
        profileImage.layer.borderWidth = 0.5
        profileImage.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor

        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.clipsToBounds = true
        mainContainerView.addSubview(profileImage)
        
        // edit button
        editButton.frame = CGRect(x: (mainContainerView.frame.size.width - sideSpace - 60), y: profileImage.frame.origin.y, width: editButtonsSize, height: editButtonsSize)
        editButton.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 0.75)
        editButton.setBackgroundImage(UIImage(named: "edit_image_50"), forState: UIControlState.Normal)
        editButton.layer.cornerRadius = editButton.frame.size.width/2
        editButton.clipsToBounds = true
        editButton.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        editButton.layer.borderWidth = 0.5
        editButton.addTarget(self, action: "handleEditButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        mainContainerView.addSubview(editButton)
        
        // profile name
        profileName.frame = CGRect(x: sideSpace, y: (profileImage.frame.origin.y + profileImage.frame.height + spacingViews + 20), width: allInOneView.frame.width, height: 20)
        profileName.tag = 3
        profileName.textAlignment = NSTextAlignment.Center
        profileName.text = self.user_first_name! + " " + self.user_last_name!
        profileName.textColor = UIColor.whiteColor()
        mainContainerView.addSubview(profileName)
        
        // profile status
        
        userStatus.frame = CGRect(x: sideSpace, y: (profileName.frame.origin.y + profileName.frame.height + spacingViews ), width: allInOneView.frame.width, height: 20)
        userStatus.tag = 4
        userStatus.textAlignment = NSTextAlignment.Center
        userStatus.text = self.user_status!
        userStatus.textColor = UIColor.whiteColor()
        mainContainerView.addSubview(userStatus)
        
        //MIDDLE PART
        
        // Label
        userPhoneNumberLabel.frame = CGRect(x: sideSpace , y: (userStatus.frame.origin.y + userStatus.frame.height + spacingViews + 10), width: 105, height: 30)
        userPhoneNumberLabel.tag = 5
        userPhoneNumberLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userPhoneNumberLabel.textAlignment = NSTextAlignment.Left
        userPhoneNumberLabel.text = " PHONE"
        userPhoneNumberLabel.textColor = UIColor.whiteColor()
        mainContainerView.addSubview(userPhoneNumberLabel)
        // TextField
        userPhoneNumber.frame = CGRect(x: (sideSpace + userPhoneNumberLabel.frame.width), y: userPhoneNumberLabel.frame.origin.y, width: (containerViewWidth-(105+20)), height: 30)
        userPhoneNumber.tag = 6
        userPhoneNumber.backgroundColor = UIColor.whiteColor()
        userPhoneNumber.textAlignment = NSTextAlignment.Center
        userPhoneNumber.textColor = UIColor.blueColor()
        userPhoneNumber.userInteractionEnabled = false
        userPhoneNumber.text = self.user_phone_number!
        mainContainerView.addSubview(userPhoneNumber)
        
        // Label
        userEmailLabel.frame = CGRect(x: sideSpace , y: (userPhoneNumberLabel.frame.origin.y + userPhoneNumberLabel.frame.height + spacingViews ), width: 105, height: 30)
        userEmailLabel.tag = 7
        userEmailLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userEmailLabel.textAlignment = NSTextAlignment.Left
        userEmailLabel.text = " EMAIL"
        userEmailLabel.textColor = UIColor.whiteColor()
        mainContainerView.addSubview(userEmailLabel)
        // TextField
        userEmail.frame = CGRect(x: (sideSpace + userPhoneNumberLabel.frame.width), y: userEmailLabel.frame.origin.y, width: (containerViewWidth-(105+20)), height: 30)
        userEmail.tag = 8
        userEmail.backgroundColor = UIColor.whiteColor()
        userEmail.textAlignment = NSTextAlignment.Center
        userEmail.textColor = UIColor.blueColor()
        userEmail.userInteractionEnabled = false
        userEmail.text = self.user_email!
        mainContainerView.addSubview(userEmail)
        
        // Label
        userGroupLabel.frame = CGRect(x: sideSpace , y: (userEmailLabel.frame.origin.y + userEmailLabel.frame.height + spacingViews ), width: 105, height: 30)
        userGroupLabel.tag = 9
        userGroupLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userGroupLabel.textAlignment = NSTextAlignment.Left
        userGroupLabel.text = " GROUP"
        userGroupLabel.textColor = UIColor.whiteColor()
        mainContainerView.addSubview(userGroupLabel)
        // TextField
        userGroup.frame = CGRect(x: (sideSpace + userPhoneNumberLabel.frame.width), y: userGroupLabel.frame.origin.y, width: (containerViewWidth-(105+20)), height: 30)
        userGroup.tag = 10
        userGroup.backgroundColor = UIColor.whiteColor()
        userGroup.textAlignment = NSTextAlignment.Center
        userGroup.textColor = UIColor.blueColor()
        userGroup.userInteractionEnabled = false
        userGroup.text = self.user_group!
        mainContainerView.addSubview(userGroup)
        
        // Label
        userAffiliationLabel.frame = CGRect(x: sideSpace , y: (userGroupLabel.frame.origin.y + userGroupLabel.frame.height + spacingViews), width: 105, height: 30)
        userAffiliationLabel.tag = 11
        userAffiliationLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userAffiliationLabel.textAlignment = NSTextAlignment.Left
        userAffiliationLabel.text = " AFFILIATION"
        userAffiliationLabel.textColor = UIColor.whiteColor()
        mainContainerView.addSubview(userAffiliationLabel)
        // TextField
        userAffiliation.frame = CGRect(x: (sideSpace + userPhoneNumberLabel.frame.width), y: userAffiliationLabel.frame.origin.y, width: (containerViewWidth-(105+20)), height: 30)
        userAffiliation.tag = 12
        userAffiliation.backgroundColor = UIColor.whiteColor()
        userAffiliation.textAlignment = NSTextAlignment.Center
        userAffiliation.textColor = UIColor.blueColor()
        userAffiliation.userInteractionEnabled = false
        //userAffiliation.text = self.user_affiliation!
        mainContainerView.addSubview(userAffiliation)
        
        // Label
        userProvinceLabel.frame = CGRect(x: sideSpace , y: (userAffiliationLabel.frame.origin.y + userAffiliationLabel.frame.size.height + spacingViews), width: 105, height: 30)
        userProvinceLabel.tag = 13
        userProvinceLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userProvinceLabel.textAlignment = NSTextAlignment.Left
        userProvinceLabel.text = " PROVINCE"
        userProvinceLabel.textColor = UIColor.whiteColor()
        mainContainerView.addSubview(userProvinceLabel)
        // TextField
        userProvince.frame = CGRect(x: (sideSpace + userProvinceLabel.frame.width) , y: userProvinceLabel.frame.origin.y, width: (containerViewWidth-(105+20)), height: 30)
        userProvince.tag = 14
        userProvince.backgroundColor = UIColor.whiteColor()
        userProvince.textAlignment = NSTextAlignment.Center
        userProvince.textColor = UIColor.blueColor()
        userProvince.userInteractionEnabled = false
        userProvince.text = self.user_province!
        mainContainerView.addSubview(userProvince)
        
        // Label
        userDistrictLabel.frame = CGRect(x: sideSpace , y: (userProvinceLabel.frame.origin.y + userProvinceLabel.frame.height + spacingViews), width: 105, height: 30)
        userDistrictLabel.tag = 15
        userDistrictLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userDistrictLabel.textAlignment = NSTextAlignment.Left
        userDistrictLabel.text = " DISTRICT"
        userDistrictLabel.textColor = UIColor.whiteColor()
        mainContainerView.addSubview(userDistrictLabel)
        // TextField
        userDistrict.frame = CGRect(x: (sideSpace + userPhoneNumberLabel.frame.width), y: userDistrictLabel.frame.origin.y, width: (containerViewWidth-(105+20)), height: 30)
        userDistrict.tag = 16
        userDistrict.backgroundColor = UIColor.whiteColor()
        userDistrict.textAlignment = NSTextAlignment.Center
        userDistrict.textColor = UIColor.blueColor()
        userDistrict.userInteractionEnabled = false
        userDistrict.text = self.user_district!
        mainContainerView.addSubview(userDistrict)
        
        // Label
        userSectorLabel.frame = CGRect(x: sideSpace , y: (userDistrictLabel.frame.origin.y + userDistrictLabel.frame.size.height + spacingViews), width: 105, height: 30)
        userSectorLabel.tag = 17
        userSectorLabel.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        userSectorLabel.textAlignment = NSTextAlignment.Left
        userSectorLabel.text = " SECTOR"
        userSectorLabel.textColor = UIColor.whiteColor()
        mainContainerView.addSubview(userSectorLabel)
        // TextField
        userSector.frame = CGRect(x: (sideSpace + userPhoneNumberLabel.frame.width), y: userSectorLabel.frame.origin.y, width: (containerViewWidth-(105+20)), height: 30)
        userSector.tag = 18
        userSector.backgroundColor = UIColor.whiteColor()
        userSector.textAlignment = NSTextAlignment.Center
        userSector.textColor = UIColor.blueColor()
        userSector.userInteractionEnabled = false
        userSector.text = self.user_sector!
        mainContainerView.addSubview(userSector)
        
    }
    
    func createButtomPart(typeOfIphone:Int, mainContainerView:UIView, containerViewWidth:CGFloat, containerViewHeight: CGFloat){
        
        var buttomViewHeight:CGFloat = 0
        switch typeOfIphone{
        case 5:
            spacingViews = 4
            buttomViewHeight = 95
            break
        case 6:
            spacingViews = 8
            buttomViewHeight = 105
            break
        case 61:
            
            break
        default:
            spacingViews = 10
        }
        
        // BOTTOM PART
        // create the white container
        bottomContainerView.frame = CGRect(x: 0, y: (mainContainerView.frame.height - buttomViewHeight), width: containerViewWidth, height: 120)
        bottomContainerView.tag = 21
        bottomContainerView.backgroundColor = UIColor.whiteColor()
        mainContainerView.addSubview(bottomContainerView)
        
        // Message Text Field
        userMessageText.frame = CGRect(x: spacingViews, y: bottomContainerView.frame.origin.y +  spacingViews*2, width: (mainContainerView.frame.width-(spacingViews*2)), height: 40)
        userMessageText.tag = 22
        userMessageText.delegate = self
        userMessageText.backgroundColor = UIColor(red: (232/255), green: (227/255), blue: (216/255), alpha: 1)
        
        //========================================
        
        userMessageText.font = UIFont.italicSystemFontOfSize(15)
        
        userMessageText.textColor = UIColor(red: (35/255), green: (158/255), blue: (218/255),alpha:1)
        mainContainerView.addSubview(userMessageText)
        
        // Send Message Button
        sendMEssage.frame = CGRect(x: ((mainContainerView.frame.width/2) - (60)), y: (mainContainerView.frame.height - (spacingViews + 30)), width: 120, height: 30)
        sendMEssage.layer.cornerRadius = 3
        sendMEssage.setTitleColor(UIColor(red: (35/255), green: (158/255), blue: (218/255),alpha:1), forState: UIControlState.Normal)
        sendMEssage.titleLabel?.font = UIFont.systemFontOfSize(15)
        sendMEssage.setTitle("Send Message", forState: UIControlState.Normal)
        sendMEssage.layer.borderColor = UIColor(red: (35/255), green: (158/255), blue: (218/255),alpha:1).CGColor
        sendMEssage.layer.borderWidth = 0.75
        sendMEssage.addTarget(self, action: "handleSendMessageButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        mainContainerView.addSubview(sendMEssage)
        
    }
    
    // Create Iphone 4 message box
    var iPhone4MessageBox:UIView = UIView(frame: CGRectZero);
    // create Iphone 4 send message button
    var composeIphone4MessageButton:UIButton = UIButton()
    func createIphone4Messagebutton(){
        composeIphone4MessageButton.frame = CGRect(x: (self.view.frame.size.width - sideSpace - 60), y: ((spacingViews*2)+40), width: editButton.frame.width, height: editButton.frame.height)
        composeIphone4MessageButton.layer.cornerRadius = editButton.frame.size.width/2
        composeIphone4MessageButton.clipsToBounds = true
        composeIphone4MessageButton.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        composeIphone4MessageButton.setBackgroundImage(UIImage(named: ""), forState: UIControlState.Normal)
        composeIphone4MessageButton.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        composeIphone4MessageButton.layer.borderWidth = 1
        composeIphone4MessageButton.addTarget(self, action: "handleMessageButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        allInOneView.addSubview(composeIphone4MessageButton)
        
    }
    
    func createIphone4MessageBox(){
        iPhone4MessageBox.frame = CGRect(x: 5, y: -(self.mainNavBar.frame.size.height+self.mainNavBar.frame.origin.y), width: (self.allInOneView.frame.width - 10), height: 150)
        iPhone4MessageBox.alpha = 0
        iPhone4MessageBox.hidden = true
        iPhone4MessageBox.layer.cornerRadius = 3
        iPhone4MessageBox.layer.borderWidth = 0.75
        iPhone4MessageBox.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        iPhone4MessageBox.backgroundColor = UIColor(red: (232/255), green: (227/255), blue: (216/255), alpha: 1)
        
        userMessageText.frame = CGRect(x: 5, y: 5, width: iPhone4MessageBox.frame.width - 10, height: 100)
        userMessageText.backgroundColor = UIColor.whiteColor()
        userMessageText.textColor = UIColor.blueColor()
        userMessageText.font = UIFont.systemFontOfSize(16, weight: 0.0)
        iPhone4MessageBox.addSubview(userMessageText)
        
        // Add cancel and send buttons
        let cancelMsgButton:UIButton = UIButton()
        cancelMsgButton.frame = CGRect(x: 5, y: (iPhone4MessageBox.frame.height - 35), width: 80, height: 30)
        cancelMsgButton.layer.cornerRadius = 3
        cancelMsgButton.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        cancelMsgButton.layer.borderWidth = 0.5
        cancelMsgButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        cancelMsgButton.setTitle("Cancel", forState: .Normal)
        cancelMsgButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        cancelMsgButton.titleLabel?.textAlignment = NSTextAlignment.Center
        cancelMsgButton.addTarget(self, action: "handleCancelButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        iPhone4MessageBox.addSubview(cancelMsgButton)
        
        let sendMsgButton:UIButton = UIButton()
        sendMsgButton.frame = CGRect(x: (iPhone4MessageBox.frame.width - (80+5)), y: (iPhone4MessageBox.frame.height - 35), width: 80, height: 30)
        sendMsgButton.layer.cornerRadius = 3
        sendMsgButton.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        sendMsgButton.layer.borderWidth = 0.5
        sendMsgButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        sendMsgButton.setTitle("Send", forState: .Normal)
        sendMsgButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        sendMsgButton.titleLabel?.textAlignment = NSTextAlignment.Left
        sendMsgButton.addTarget(self, action: "handleSendMessageButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        iPhone4MessageBox.addSubview(sendMsgButton)
        
        
        self.view.addSubview(iPhone4MessageBox)
    }
    
    func openIphone4MesgBox()
    {
        self.iPhone4MessageBox.hidden = false
        
        UIView.animateWithDuration(0.5,
            animations: {
                self.iPhone4MessageBox.frame = CGRect(x: 5, y: (self.mainNavBar.frame.size.height+self.mainNavBar.frame.origin.y), width: (self.allInOneView.frame.width - 10), height: 150)
                self.userMessageText.text = ""
                self.userMessageText.becomeFirstResponder()
                self.iPhone4MessageBox.alpha = 0.9
        })
    }
    
    func closeIphone4MesgBox()
    {
        UIView.animateWithDuration(0.5,
            animations: {
                self.iPhone4MessageBox.frame = CGRect(x: 5, y: -(self.mainNavBar.frame.size.height+self.mainNavBar.frame.origin.y), width: (self.allInOneView.frame.width - 10), height: 150)
                self.userMessageText.resignFirstResponder()
                self.iPhone4MessageBox.alpha = 0
            },
            completion: { finished in
                
                self.iPhone4MessageBox.hidden = true
            }
        )
    }
    
    // Handle Button clicks
    // Send Message function
    @IBAction func handleMessageButtonClick(sender: UIButton){
        closePicker()
        createIphone4MessageBox()
        
        iPhone4MessageBox.hidden ? openIphone4MesgBox() : closeIphone4MesgBox()
    }
    
    func handleCancelButtonClick(sender: UIButton){
        closeIphone4MesgBox()
    }
    
    // Handle Edit Button click
    @IBAction func handleEditButtonClick(sender: UIButton){
        performSegueWithIdentifier("showEditPage", sender: nil)
    }
    
    func handleSendMessageButtonClick(sender:UIButton){
        closePicker()
        if userMessageText.text.isEmpty {
            showAlertController("Message box is empty")
        }
        else{
            closeIphone4MesgBox()
            // send the message
        }
    }
    
    // user status
    func getUserMessageText()->String{
        var gotTheMessage:String = ""
        if( !userMessageText.text.isEmpty){
            gotTheMessage = userMessageText.text
        }
        return gotTheMessage
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "About"],
            ["title" : "Settings"],
            ["title" : "Share"],
            ["title" : "Sign Out"]
        ]
    }
    
    @IBAction func pickerSelect(sender: UIBarButtonItem)
    {
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createPicker()
    {
        pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        pickerMenu.layer.borderWidth = 0.75
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func openPicker()
    {
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    func buttonClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/360), green: (47/360), blue: (61/360), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        switch sender.tag{
        case 0: // About
            performSegueWithIdentifier("fromUserPageToAbout", sender: self)
            break
        case 1: // Settings
            performSegueWithIdentifier("fromUserPageToSettings", sender: self)
            break
        case 2: // Share
            performSegueWithIdentifier("fromUserPageToShare", sender: self)
            break
        case 3: // Sign out
            break
        default:
            println("No such segue exists in your app!")
        }
        
        closePicker()
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    var kbHeight: CGFloat!
    var textViewTag: Int = -1
    
    func animateTextField(up: Bool) {
        var movement = (up ? -kbHeight : kbHeight)
        
        UIView.animateWithDuration(0.3, animations: {
            self.view.frame = CGRectOffset(self.view.frame, 0, movement)
        })
    }
    
    override func viewWillAppear(animated:Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                var textView:UITextView = UITextView()
                if self.view.viewWithTag(textViewTag) is UITextView{
                    textView = self.view.viewWithTag(textViewTag) as! UITextView
                }
                if((keyboardSize.height + 25) > (self.view.frame.height - textView.frame.origin.y)){
                    kbHeight = (keyboardSize.height - (self.view.frame.height - textView.frame.origin.y)) + 85
                    
                }
                else{
                    kbHeight = 0
                }
                self.animateTextField(true)
            }
            
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.animateTextField(false)
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    
    // =================== Handle Save Button =======================
    func showAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }    // =================== Handle Save Button =======================
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        closePicker()
        self.view.endEditing(true)
        
        textViewTag = textView.tag
        return true
    }
    
    // Handle touches done outside the input fields
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        closePicker()
        self.view.endEditing(true)
    }
    
    @IBAction func saveUserChanges(segue:UIStoryboardSegue) {
        let sourceViewController = segue.sourceViewController as! editPageViewController
        // transfer user data
        self.user_image = sourceViewController.user_image!
        self.user_first_name = sourceViewController.user_first_name!
        self.user_last_name = sourceViewController.user_last_name!
        self.user_email = sourceViewController.user_email!
        self.user_status = sourceViewController.user_status!
        self.user_affiliation = sourceViewController.user_affiliation!
        self.user_phone_number = sourceViewController.user_phone_number!
        self.user_group = sourceViewController.user_group!
        self.user_province = sourceViewController.user_province!
        self.user_district = sourceViewController.user_district!
        self.user_sector = sourceViewController.user_sector!
        
        saveChanges()
    }
    
    func saveChanges() {
        profileImage.image = self.user_image!
        profileName.text = self.user_first_name! + " " + self.user_last_name!
        userStatus.text  = self.user_status!
        userProvince.text = self.user_province!
        userDistrict.text = self.user_district!
        userSector.text = self.user_sector!
        userPhoneNumber.text = self.user_phone_number!
        userEmail.text = self.user_email!
        userGroup.text = self.user_group!
        userAffiliation.text = self.user_affiliation!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showEditPage" {
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! editPageViewController
            toViewController.iphoneType = self.iphoneType!
            toViewController.transitioningDelegate = self.transitionManager
            
            // transfer user data
            toViewController.user_image = self.user_image!
            toViewController.user_email = self.user_email!
            toViewController.user_password = self.user_password!
            toViewController.user_gender = self.user_gender!
            toViewController.user_status = self.user_status!
            toViewController.user_first_name = self.user_first_name!
            toViewController.user_last_name = self.user_last_name!
            toViewController.user_affiliation = self.user_affiliation!
            toViewController.user_phone_number = self.user_phone_number!
            toViewController.user_group = self.user_group!
            toViewController.user_subgroup = self.user_subgroup!
            toViewController.user_province = self.user_province!
            toViewController.user_district = self.user_district!
            toViewController.user_sector = self.user_sector!
        }
    }
}
