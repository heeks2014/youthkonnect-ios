//
//  customButtonForBrownViews.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 5/29/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class customButtonForBrownViews: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.borderColor = UIColor(red: (32/360), green: (172/360), blue: (209/360), alpha: 1).CGColor
        self.layer.borderWidth = 0.75
        self.tintColor = UIColor(red: (32/360), green: (172/360), blue: (209/360), alpha: 1)
        self.layer.cornerRadius = 3
    }

}
