//
//  slidInAndOut.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 6/8/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class slidInAndOut: UIStoryboardSegue {
    override func perform() {
        var source:UIViewController = self.sourceViewController as! UIViewController
        var destination:UIViewController = self.destinationViewController as! UIViewController
        
        source.navigationController?.pushViewController(destinationViewController as! UIViewController, animated: true)
    }
}
