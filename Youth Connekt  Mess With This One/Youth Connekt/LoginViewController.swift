//
//  LoginViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 5/15/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class LoginViewController:UIViewController,UITextFieldDelegate {
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    
    @IBOutlet weak var mainNavBar: UINavigationBar!
    var containerView:UIView = UIView()
    
    // Screen Properties
    var spacingViews:CGFloat = 4
    let sideSpace:CGFloat = 40
    
    // Outlets
    var youthConnektLogo:UIImageView = UIImageView()
    var emailTextField:UITextField = UITextField()
    var passwordTextField:UITextField = UITextField()
    var signInButton:UIButton = UIButton()
    var signUpButton:UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        createViews(typeOfIphone(), containerViewHeight: (self.view.frame.height - (mainNavBar.frame.origin.y + mainNavBar.frame.height)),containerViewWidth: self.view.frame.width)
        createPicker()
    }
    
    func typeOfIphone()->Int{
        var typeOfIphone:Int = 0
        if UIScreen.mainScreen().bounds.size.width > 320 {
            if UIScreen.mainScreen().scale == 3 {
                // Working on iPhone 6 plus
                //-------------------------
                typeOfIphone = 61
                
            } else {
                // Working on iPhone 6
                //--------------------
                typeOfIphone = 6
            }
        } else {
            // Working on iPhone 4s and 5z
            //----------------------------
            if(UIScreen.mainScreen().bounds.size.height > 480){
                typeOfIphone = 5
            }
            else{
                typeOfIphone = 4
            }
        }
        return typeOfIphone
    }
    
    func createViews(typeOfIphone:Int, containerViewHeight:CGFloat, containerViewWidth:CGFloat)
    {
        var youthConnektLogoSize:CGFloat = 0
        var spaceLogo_NavBar:CGFloat = 0
        var middlePartOffset:CGFloat = 0
        
        switch typeOfIphone{
        case 4:
            youthConnektLogoSize = 90
            spaceLogo_NavBar += 15
            middlePartOffset = 10
            spacingViews = 4
            break
        case 5:
            youthConnektLogoSize = 120
            spaceLogo_NavBar += 25
            middlePartOffset = 20
            spacingViews = 8
            break
        case 6:
            youthConnektLogoSize = 150
            spaceLogo_NavBar += 35
            middlePartOffset = 20
            spacingViews = 12
            break
        case 61:
            youthConnektLogoSize = 150
            spaceLogo_NavBar += 35
            middlePartOffset = 20
            spacingViews = 12
            
            break
        default:
            println("choose Iphone!")
        }
        
        containerView.frame = CGRect(x: 0, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height), width: containerViewWidth, height: containerViewHeight)
        containerView.backgroundColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1)
        self.view.addSubview(containerView)
        
        emailTextField.frame = CGRect(x: sideSpace, y: (containerView.frame.height/2 - 50), width: containerView.frame.width - (sideSpace*2), height: 30)
        emailTextField.delegate = self
        emailTextField.tag = 1
        emailTextField.placeholder = "email"
        emailTextField.textColor = UIColor.whiteColor()
        
        var emailBorder = CALayer()
        var width = CGFloat(0.75)
        emailBorder.borderColor = UIColor.whiteColor().CGColor
        emailBorder.borderWidth = width
        emailBorder.frame = CGRectMake(0, emailTextField.frame.size.height - width, emailTextField.frame.size.width, emailTextField.frame.size.height)
        emailTextField.layer.addSublayer(emailBorder)
        emailTextField.layer.masksToBounds = true
        
        containerView.addSubview(emailTextField)
        
        youthConnektLogo.frame = CGRect(x: ((containerView.frame.width / 2) - (youthConnektLogoSize/2)), y: spaceLogo_NavBar + spaceLogo_NavBar, width: youthConnektLogoSize , height: youthConnektLogoSize)
        youthConnektLogo.image = UIImage(named:"YouthConnekt_Logo")
        containerView.addSubview(youthConnektLogo)
        
        passwordTextField.frame = CGRect(x: sideSpace, y: containerView.frame.height/2, width: containerView.frame.width - (sideSpace*2), height: 30)
        passwordTextField.delegate = self
        
        passwordTextField.tag = 2
        passwordTextField.placeholder = "passsword"
        passwordTextField.textColor = UIColor.whiteColor()
        passwordTextField.secureTextEntry = true
        
        var passwordBorder = CALayer()
        passwordBorder.borderColor = UIColor.whiteColor().CGColor
        passwordBorder.borderWidth = width
        passwordBorder.frame = CGRectMake(0, passwordTextField.frame.size.height - width, passwordTextField.frame.size.width, passwordTextField.frame.size.height)
        passwordTextField.layer.addSublayer(passwordBorder)
        passwordTextField.layer.masksToBounds = true
        
        containerView.addSubview(passwordTextField)
        
        signInButton.frame = CGRect(x: (containerView.frame.width/2 - 64), y: (passwordTextField.frame.origin.y + passwordTextField.frame.height + 30), width: 128, height: 30)
        signInButton.tag = 3
        signInButton.layer.borderColor = UIColor.whiteColor().CGColor
        signInButton.layer.borderWidth = 1
        signInButton.layer.cornerRadius = 3
        signInButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        signInButton.setTitle("Sign In", forState: .Normal)
        signInButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        signInButton.titleLabel?.textAlignment = NSTextAlignment.Center
        signInButton.addTarget(self, action: "signInButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(signInButton)
        
        signUpButton.frame = CGRect(x: (containerView.frame.width/2 - 64), y: (signInButton.frame.origin.y + signInButton.frame.height), width: 128, height: 30)
        signUpButton.tag = 4
        signUpButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        signUpButton.setTitle("New? Sign Up", forState: .Normal)
        signUpButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        signUpButton.titleLabel?.textAlignment = NSTextAlignment.Center
        signUpButton.addTarget(self, action: "signUpButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(signUpButton)
    }
    
    // button click handlers
    func signInButtonClick(sender:UIButton){
        closePicker()
        if !emailTextField.text.isEmail && passwordTextField.text.isEmpty {
            showAlertController("Please enter your email and your password!")
        }
        else if !emailTextField.text.isEmail{
            showAlertController("Please enter your correct email!")
        }
        else {
            showAlertController("Yay! Do a segue!")
        }
    }
    
    func signUpButtonClick(sender:UIButton){
        //showAlertController("Yay! Do a segue!")
        closePicker()
        performSegueWithIdentifier("goToSignUp", sender: self)
    }
    
    func showAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField == emailTextField{
            if !emailTextField.text.isEmail{
                emailTextField.textColor = UIColor.redColor()
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    var kbHeight: CGFloat!
    var textFieldTag: Int = -1
    
    func animateTextField(up: Bool) {
        var movement = (up ? -kbHeight : kbHeight)
        
        UIView.animateWithDuration(0.3, animations: {
            self.containerView.frame = CGRectOffset(self.containerView.frame, 0, movement)
        })
    }
    
    override func viewWillAppear(animated:Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                let textfield:UITextField = self.view.viewWithTag(textFieldTag) as! UITextField
                if((keyboardSize.height + 25) > (self.containerView.frame.height - textfield.frame.origin.y)){
                    kbHeight = (keyboardSize.height - (self.containerView.frame.height - textfield.frame.origin.y)) + 35
                    
                }
                else{
                    kbHeight = 0
                }
                self.animateTextField(true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.animateTextField(false)
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "About"],
            ["title" : "Settings"],
            ["title" : "Share"],
            ["title" : "Sign In"]
        ]
    }
    
    @IBAction func pickerSelect(sender: UIBarButtonItem)
    {
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createPicker()
    {
        pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func openPicker()
    {
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    func buttonClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/255), green: (47/255), blue: (61/255), alpha: 1)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        switch sender.tag{
        case 0: // About
            performSegueWithIdentifier("fromLoginToAbout", sender: self)
            break
        case 1: // Settings
            performSegueWithIdentifier("fromLoginToSettings", sender: self)
            break
        case 2: // Share
            performSegueWithIdentifier("fromLoginToShare", sender: self)
            break
        case 3: // Sign In
            
            break
        default:
            println("No such segue exists in your app!")
        }
        
        closePicker()
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        textFieldTag = textField.tag
        closePicker()
        return true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        closePicker()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToSignUp"  {
            
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! registerViewController
            toViewController.iphoneType = self.typeOfIphone()
            toViewController.transitioningDelegate = self.transitionManager
            
        }
    }
}

// Add a class extension for email validation
extension String {
    var isEmail: Bool {
        let regex = NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: .CaseInsensitive, error: nil)
        return regex?.firstMatchInString(self, options: nil, range: NSMakeRange(0, count(self))) != nil
    }
}

// Add a class extension for phone number validation
extension String{
    var isPhoneNumber:Bool {
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        var phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        return phoneTest.evaluateWithObject(self)
    }
}