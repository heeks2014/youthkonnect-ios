//
//  customTextField.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 5/22/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class customTextField: UITextField {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        var border = CALayer()
        var width = CGFloat(0.75)
        border.borderColor = UIColor.whiteColor().CGColor
        border.frame = CGRectMake(0, self.frame.size.height - width, self.frame.size.width, self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }

}
