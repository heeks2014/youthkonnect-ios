//
//  welcomeIstPageViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 6/8/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class welcomeIstPageViewController: UIViewController {
    
    // -------------- user data-----------------
    var user_name:String?
    var user_first_name:String?
    var user_last_name:String?
    var user_gender:String?
    var user_status:String?
    var user_image:UIImage?
    var user_phone_number:String?
    var user_email:String?
    var user_password:String?
    var user_group:String?
    var user_subgroup:String?
    var user_affiliation:String?
    var user_province:String?
    var user_district:String?
    var user_sector:String?
    // -------------- user data-----------------
    
    // type of iphone
    var iphoneType:Int?
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    @IBOutlet weak var containerSubView: UIView!
    @IBOutlet weak var mainNavBar: UINavigationBar!
    
    @IBOutlet weak var userEmailLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        mainNavBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        containerSubView.frame = CGRect(x: 0, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height), width: self.view.frame.width, height: self.view.frame.height)
        createPicker()
        userEmailLabel.text = "email@host.domainName"
    }
    
    @IBAction func groupLeaderButton(sender: AnyObject) {
        self.user_status = "Leader"
        performSegueWithIdentifier("goToGroupLeaderPage", sender: self)
    }
    
    @IBAction func groupMemberButton(sender: AnyObject) {
        self.user_status = "Member"
        performSegueWithIdentifier("goToGroupMemberPage", sender: self)
    }

    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "About"],
            ["title" : "Settings"],
            ["title" : "Share"],
            ["title" : "Sign In"]
        ]
    }
    
    @IBAction func pickerSelect(sender: UIBarButtonItem)
    {
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createPicker()
    {
        pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        pickerMenu.layer.borderWidth = 0.75
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func openPicker()
    {
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    func buttonClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/360), green: (47/360), blue: (61/360), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        switch sender.tag{
        case 0: // About
            performSegueWithIdentifier("fromWelcomeIstPageToAbout", sender: self)
            break
        case 1: // Settings
            performSegueWithIdentifier("fromWelcomeIstPageToSettings", sender: self)
            break
        case 2: // Share
            performSegueWithIdentifier("fromWelcomeIstPageToShare", sender: self)
            break
        case 3: // Sign In
            
            break
        default:
            println("No such segue exists in your app!")
        }
        
        closePicker()
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToGroupMemberPage" {
            
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! memberBasicInfoViewController
            toViewController.iphoneType = self.iphoneType!
            toViewController.transitioningDelegate = self.transitionManager
            
            // transfer user data
            toViewController.user_email = self.user_email!
            toViewController.user_password = self.user_password!
            toViewController.user_gender = self.user_gender!
            toViewController.user_status = self.user_status!
        }
        else if segue.identifier == "goToGroupLeaderPage" {
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! addingANewGroupViewController
            toViewController.iphoneType = self.iphoneType!
            toViewController.transitioningDelegate = self.transitionManager
            
            // transfer user data
            toViewController.user_email = self.user_email!
            toViewController.user_password = self.user_password!
            toViewController.user_gender = self.user_gender!
            toViewController.user_status = self.user_status!
            
        }
    }
    
    // unwind the segue
    @IBAction func handleBackButtonClick(sender:UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
