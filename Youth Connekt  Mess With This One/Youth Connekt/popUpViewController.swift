//
//  popUpViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 5/23/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class popUpViewController: UIViewController {
    
    @IBAction func aboutBtnPressed(sender: UIButton) {
        sender.backgroundColor = UIColor(red: 0.091, green: 0.130, blue: 0.1694, alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    }
    
    @IBAction func settingsBtnPressed(sender: UIButton) {
        sender.backgroundColor = UIColor(red: 0.091, green: 0.130, blue: 0.1694, alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    }
    
    @IBAction func shareBtnPressed(sender: UIButton) {
        sender.backgroundColor = UIColor(red: 0.091, green: 0.130, blue: 0.1694, alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    }
    
    @IBAction func signInBtnPressed(sender: UIButton) {
        sender.backgroundColor = UIColor(red: 0.091, green: 0.130, blue: 0.1694, alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.view.superview!.layer.cornerRadius = 2
    }
    override var preferredContentSize: CGSize {
        get {
            return CGSize(width: 116, height: 117)
        }
        set {
            super.preferredContentSize = newValue
        }
    }
}
