//
//  memberBasicInfoViewController.swift
//  Youth Connekt
//
//  Created by Olivier Tuyishime on 5/29/15.
//  Copyright (c) 2015 Olivier Tuyishime. All rights reserved.
//

import UIKit

class memberBasicInfoViewController: UIViewController,UITextFieldDelegate {
    
    // -------------- user data-----------------
    var user_name:String?
    var user_first_name:String?
    var user_last_name:String?
    var user_gender:String?
    var user_status:String?
    var user_image:UIImage?
    var user_phone_number:String?
    var user_email:String?
    var user_password:String?
    var user_group:String?
    var user_subgroup:String?
    var user_affiliation:String?
    var user_province:String?
    var user_district:String?
    var user_sector:String?
    // -------------- user data-----------------
    
    // ================== test transition manger ================
    // add this right above your viewDidLoad function...
    let transitionManager = TransitionManager()
    //===========================================================
    
    @IBOutlet weak var mainNavBar: UINavigationBar!
    var containerView:UIView = UIView()
    
    // Screen Properties
    var spacingViews:CGFloat = 0
    let sideSpace:CGFloat = 40
    
    // Outlets
    var titleLabel:UILabel = UILabel()
    var firstNameTextField:UITextField = UITextField()
    var lasttNameTextField:UITextField = UITextField()
    var emailTextField:UITextField = UITextField()
    var phoneTextField:UITextField = UITextField()
    var groupTextfield:UITextField = UITextField()
    var groupDropDownButton:UIButton = UIButton()
    
    var subGroupTextField:UITextField = UITextField()
    var subGroupDropDown:UIButton = UIButton()
    
    var nextButton:UIButton = UIButton()
    
    // type of iphone
    var iphoneType:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        createViews(iphoneType!, containerViewHeight: (self.view.frame.height - (mainNavBar.frame.origin.y + mainNavBar.frame.height)),containerViewWidth: self.view.frame.width)
        createPicker()
        createDropDownList()
    }
    
    func createViews(typeOfIphone:Int, containerViewHeight:CGFloat, containerViewWidth:CGFloat)
    {
        var youthConnektLogoSize:CGFloat = 0
        var spaceLogo_NavBar:CGFloat = 0
        var middlePartOffset:CGFloat = 0
        
        switch typeOfIphone{
        case 4:
            spacingViews = 20
            break
        case 5:
            spacingViews = 30
            break
        case 6:
            spacingViews = 35
            break
        case 61:
            spacingViews = 40
            
            break
        default:
            spacingViews = 10
        }
        
        containerView.frame = CGRect(x: 0, y: (mainNavBar.frame.origin.y + mainNavBar.frame.height), width: containerViewWidth, height: containerViewHeight)
        containerView.backgroundColor = UIColor(red: (232/255), green: (227/255), blue: (216/255), alpha: 1)
        self.view.addSubview(containerView)
        
        //============================ titleLabel ===================================
        titleLabel.frame = CGRect(x: ((containerView.frame.width / 2) - 75), y: spacingViews, width: 150, height: 30)
        titleLabel.tag = 1
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.font = UIFont.systemFontOfSize(20, weight: 0.1)
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.text = "Group Member"
        
        containerView.addSubview(titleLabel)
        //============================ titleLabel ===================================
        
        //============================ firstNameTextField ===================================
        firstNameTextField.frame = CGRect(x: sideSpace, y: (titleLabel.frame.origin.y + titleLabel.frame.height + (spacingViews*2)), width: containerView.frame.width - (sideSpace*2), height: 30)
        firstNameTextField.delegate = self
        firstNameTextField.delegate = self
        firstNameTextField.tag = 2
        firstNameTextField.placeholder = " first name"
        firstNameTextField.textColor = UIColor.blueColor()
        
        var firstNameBorder = CALayer()
        var width = CGFloat(0.75)
        firstNameBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        firstNameBorder.borderWidth = width
        firstNameBorder.frame = CGRectMake(0, firstNameTextField.frame.size.height - width, firstNameTextField.frame.size.width, firstNameTextField.frame.size.height)
        firstNameTextField.layer.addSublayer(firstNameBorder)
        firstNameTextField.layer.masksToBounds = true
        
        containerView.addSubview(firstNameTextField)
        //============================ firstNameTextField ===================================
        
        lasttNameTextField.frame = CGRect(x: sideSpace, y: (firstNameTextField.frame.origin.y + firstNameTextField.frame.height + spacingViews), width: containerView.frame.width - (sideSpace*2), height: 30)
        lasttNameTextField.delegate = self
        lasttNameTextField.delegate = self
        lasttNameTextField.tag = 2
        lasttNameTextField.placeholder = " last name"
        lasttNameTextField.textColor = UIColor.blueColor()
        
        var lastNameBorder = CALayer()
        lastNameBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        lastNameBorder.borderWidth = width
        lastNameBorder.frame = CGRectMake(0, lasttNameTextField.frame.size.height - width, lasttNameTextField.frame.size.width, lasttNameTextField.frame.size.height)
        lasttNameTextField.layer.addSublayer(lastNameBorder)
        lasttNameTextField.layer.masksToBounds = true
        
        containerView.addSubview(lasttNameTextField)
        
        //============================ emailTextField ===================================
        emailTextField.frame = CGRect(x: sideSpace, y: (lasttNameTextField.frame.origin.y + lasttNameTextField.frame.height + spacingViews), width: containerView.frame.width - (sideSpace*2), height: 30)
        emailTextField.delegate = self
        
        emailTextField.tag = 3
        emailTextField.placeholder = "email"
        emailTextField.text = self.user_email
        emailTextField.textColor = UIColor.blueColor()
        
        var emailBorder = CALayer()
        emailBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        emailBorder.borderWidth = width
        emailBorder.frame = CGRectMake(0, emailTextField.frame.size.height - width, emailTextField.frame.size.width, emailTextField.frame.size.height)
        emailTextField.layer.addSublayer(emailBorder)
        emailTextField.layer.masksToBounds = true
        
        containerView.addSubview(emailTextField)
        //============================ emailTextField ===================================
        
        //============================ phoneTextField ===================================
        phoneTextField.frame = CGRect(x: sideSpace, y: (emailTextField.frame.origin.y + emailTextField.frame.height + spacingViews) , width: containerView.frame.width - (sideSpace*2), height: 30)
        phoneTextField.delegate = self
        
        phoneTextField.tag = 4
        phoneTextField.placeholder = "phone"
        phoneTextField.textColor = UIColor.blueColor()
        
        var phoneBorder = CALayer()
        phoneBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        phoneBorder.borderWidth = width
        phoneBorder.frame = CGRectMake(0, phoneTextField.frame.size.height - width, phoneTextField.frame.size.width, phoneTextField.frame.size.height)
        phoneTextField.layer.addSublayer(phoneBorder)
        phoneTextField.layer.masksToBounds = true
        
        containerView.addSubview(phoneTextField)
        //============================ phoneTextField ===================================
        
        //============================ groupTextfield ===================================
        groupTextfield.frame = CGRect(x: sideSpace, y: (phoneTextField.frame.origin.y + phoneTextField.frame.height + spacingViews), width: containerView.frame.width - (sideSpace*2) - 20, height: 30)
        groupTextfield.delegate = self
        groupTextfield.userInteractionEnabled = false
        groupTextfield.tag = 5
        groupTextfield.placeholder = "group"
        groupTextfield.textColor = UIColor.blueColor()
        
        var groupBorder = CALayer()
        groupBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        groupBorder.borderWidth = width
        groupBorder.frame = CGRectMake(0, groupTextfield.frame.size.height - width, groupTextfield.frame.size.width, groupTextfield.frame.size.height)
        groupTextfield.layer.addSublayer(groupBorder)
        groupTextfield.layer.masksToBounds = true
        
        containerView.addSubview(groupTextfield)
        //============================ groupTextfield ===================================
        
        //============================ groupDropDownButton ===================================
        groupDropDownButton.frame = CGRect(x: (groupTextfield.frame.origin.x + groupTextfield.frame.width - 2), y: groupTextfield.frame.origin.y, width: 22, height: 30)
        groupDropDownButton.tag = 7
        groupDropDownButton.layer.cornerRadius = 3
        groupDropDownButton.setBackgroundImage(UIImage(named: "dropDown-50-blue"), forState: UIControlState.Normal)
        
        var groupDropDownBorder = CALayer()
        groupDropDownBorder.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        groupDropDownBorder.borderWidth = width
        groupDropDownBorder.frame = CGRectMake(0, groupDropDownButton.frame.size.height - width, groupDropDownButton.frame.size.width, groupDropDownButton.frame.size.height)
        groupDropDownButton.layer.addSublayer(groupDropDownBorder)
        groupDropDownButton.layer.masksToBounds = true
        groupDropDownButton.addTarget(self, action: "dropDownTheList:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(groupDropDownButton)
        //============================ groupDropDownButton ===================================
        
        
        //============================ nextButton ===================================
        nextButton.frame = CGRect(x: (containerView.frame.width/2 - 55), y:(containerView.frame.height - 70), width: 110, height: 30)
        nextButton.tag = 7
        nextButton.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        nextButton.layer.borderWidth = 1
        nextButton.layer.cornerRadius = 3
        nextButton.setTitleColor(UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1), forState: .Normal)
        nextButton.setTitle("Next", forState: .Normal)
        nextButton.titleLabel?.font = UIFont.systemFontOfSize(15)
        nextButton.titleLabel?.textAlignment = NSTextAlignment.Center

        nextButton.addTarget(self, action: "nextButtonClick:", forControlEvents: UIControlEvents.TouchUpInside)
        
        containerView.addSubview(nextButton)
        //============================ nextButton ===================================
    }
    
    // button click handlers
    func nextButtonClick(sender:UIButton){
        if !emailTextField.text.isEmail {
            showAlertController("You entered a wrong email and phone number! ")
        }
        else if !phoneTextField.text.isPhoneNumber{
            showAlertController("You entered a wrong phone number! ")
        }
        else{
            // ===================== Segue to next page =======================
            
            self.user_first_name = firstNameTextField.text
            self.user_last_name = lasttNameTextField.text
            self.user_email = emailTextField.text
            self.user_phone_number = phoneTextField.text
            self.user_group = groupTextfield.text
            
            performSegueWithIdentifier("goToMemberWelcomePage", sender: self)
            
        }
    }
    
    func showAlertController(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        switch textField{
        case emailTextField:
            if !emailTextField.text.isEmail{
                emailTextField.textColor = UIColor.redColor()
            }
            else{
                emailTextField.textColor = UIColor.blueColor()
            }
            break
        case phoneTextField:
            if !phoneTextField.text.isPhoneNumber{
                phoneTextField.textColor = UIColor.redColor()
            }
            else{
                phoneTextField.textColor = UIColor.blueColor()
            }
            break
        default:
            println("textfield failed")
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    var kbHeight: CGFloat!
    var textFieldTag: Int = -1
    
    func animateTextField(up: Bool) {
        var movement = (up ? -kbHeight : kbHeight)
        
        UIView.animateWithDuration(0.3, animations: {
            self.containerView.frame = CGRectOffset(self.containerView.frame, 0, movement)
        })
    }
    
    override func viewWillAppear(animated:Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                
                let textfield:UITextField = self.view.viewWithTag(textFieldTag) as! UITextField
                if((keyboardSize.height + 25) > (self.containerView.frame.height - textfield.frame.origin.y)){
                    kbHeight = (keyboardSize.height - (self.containerView.frame.height - textfield.frame.origin.y)) + 35
                    
                }
                else{
                    kbHeight = 0
                }
                self.animateTextField(true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.animateTextField(false)
    }
    
    //------------------------------------------------------------------------------------------
    // KeyBoard Hide and slide view up
    //------------------------------------------------------------------------------------------
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    let pickerMenu = UIView(frame: CGRectZero)
    
    struct menuBtnStruct {
        static let btnProperties = [
            ["title" : "About"],
            ["title" : "Settings"],
            ["title" : "Share"],
            ["title" : "Sign In"]
        ]
    }
    
    @IBAction func pickerSelect(sender: UIBarButtonItem)
    {
        pickerMenu.hidden ? openPicker() : closePicker()
    }
    
    func createPicker()
    {
        pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
        pickerMenu.alpha = 0
        pickerMenu.hidden = true
        pickerMenu.backgroundColor = UIColor.whiteColor()
        pickerMenu.layer.borderWidth = 0.75
        pickerMenu.layer.borderColor = UIColor(red: (103/255), green: (172/255), blue: (209/255), alpha: 1).CGColor
        pickerMenu.userInteractionEnabled = true
        
        var offset = 0
        for (index, name) in enumerate(menuBtnStruct.btnProperties)
        {
            let button = UIButton()
            button.frame = CGRect(x: 0, y: offset, width: 100, height: 30)
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            button.setTitle(name["title"], forState: .Normal)
            button.titleLabel?.font = UIFont.systemFontOfSize(15)
            button.titleLabel?.textAlignment = NSTextAlignment.Left
            button.addTarget(self, action: "buttonClick:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = index
            
            pickerMenu.addSubview(button)
            
            offset += 30
        }
        
        view.addSubview(pickerMenu)
    }
    
    func openPicker()
    {
        self.pickerMenu.hidden = false
        
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 1
        })
    }
    
    func closePicker()
    {
        UIView.animateWithDuration(0.3,
            animations: {
                self.pickerMenu.frame = CGRect(x: (self.view.frame.width - 150), y: 49, width: 100, height: 120)
                self.pickerMenu.alpha = 0
            },
            completion: { finished in
                self.pickerMenu.hidden = true
            }
        )
    }
    
    func buttonClick(sender: UIButton){
        sender.backgroundColor = UIColor(red: (33/360), green: (47/360), blue: (61/360), alpha: 0.85)
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        switch sender.tag{
        case 0: // About
            performSegueWithIdentifier("fromGroupMemberToAbout", sender: self)
            break
        case 1: // Settings
            performSegueWithIdentifier("fromGroupMemberToSettings", sender: self)
            break
        case 2: // Share
            performSegueWithIdentifier("fromGroupMemberToShare", sender: self)
            break
        case 3: // Sign In
            
            break
        default:
            println("No such segue exists in your app!")
        }
        
        closePicker()
    }
    
    //---------------------------------------------------------------------------------------------
    // Menu Section
    //---------------------------------------------------------------------------------------------
    
    // random info
    let infoArray:Array<String> = ["Scouts","YCS","CICR","Xaveri","SJM"]
    //------------------------------------------------------------------------------------------
    // Drop Down Section
    //------------------------------------------------------------------------------------------
    let dropDownView:UIView = UIView()
    var scrollView = UIScrollView()
    var textField = UITextField()
    
    @IBAction func dropDownTheList(sender: UIButton)
    {
        dropDownView.hidden ? openList() : closeList()
    }
    
    var pickerHeight:CGFloat = 0
    func createDropDownList()
    {
        var contentSize:CGFloat = (CGFloat)(infoArray.count*30)
        if infoArray.count*30 > 150 {
            pickerHeight = 150
        }
        else{
            pickerHeight = (CGFloat)(infoArray.count*30)
        }
        
        if ((pickerHeight + 10) > (self.view.frame.height - (groupTextfield.frame.origin.y + groupTextfield.frame.height))){
            dropDownView.frame = CGRect(x: self.groupTextfield.frame.origin.x, y: (self.groupTextfield.frame.origin.y - pickerHeight) + 10, width: self.groupTextfield.frame.width, height: pickerHeight)
            
            dropDownView.layer.cornerRadius = 3
            dropDownView.alpha = 0
            dropDownView.hidden = true
            dropDownView.backgroundColor = UIColor.whiteColor()
            dropDownView.layer.borderWidth = 0.5
            dropDownView.layer.borderColor = UIColor.blackColor().CGColor
            dropDownView.userInteractionEnabled = true
            view.addSubview(dropDownView)
            
        }
        else{
            
            // create the picker drop down view
            dropDownView.frame = CGRect(x: self.groupTextfield.frame.origin.x, y: (self.groupTextfield.frame.origin.y + self.groupTextfield.frame.height) , width: self.groupTextfield.frame.width, height: pickerHeight)
            
            dropDownView.layer.cornerRadius = 3
            dropDownView.alpha = 0
            dropDownView.hidden = true
            dropDownView.backgroundColor = UIColor.whiteColor()
            dropDownView.layer.borderWidth = 0.5
            dropDownView.layer.borderColor = UIColor.blackColor().CGColor
            dropDownView.userInteractionEnabled = true
            view.addSubview(dropDownView)
        }
        
        // Add a scroll view to it
        scrollView.frame = CGRect(x: 0, y: 0, width: dropDownView.bounds.width, height: pickerHeight)
        //scrollView.layer.cornerRadius = 3
        scrollView.backgroundColor = UIColor.whiteColor()
        scrollView.contentSize = CGSize(width: dropDownView.bounds.width,height: contentSize)
        dropDownView.addSubview(scrollView)
        
        // Add labels for the user selection
        var offSet:CGFloat = 0
        
        for( var index = 0; index < infoArray.count; index++){
            let label:UILabel = UILabel()
            label.frame = CGRect(x: 0, y: offSet, width: scrollView.frame.width, height: 30)
            label.layer.borderWidth = 0.25
            label.layer.borderColor = UIColor.grayColor().CGColor
            label.tag = index+10
            label.backgroundColor = UIColor.whiteColor()
            label.text = infoArray[index]
            label.textAlignment = NSTextAlignment.Center
            label.userInteractionEnabled = true
            
            let labelTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleLabelTap:"))
            label.addGestureRecognizer(labelTap)
            
            scrollView.addSubview(label)
            offSet += 30
        }
    }
    
    func handleLabelTap(sender:UITapGestureRecognizer){
        var tochedTag:Int = sender.view!.tag
        let textFromLabel:UILabel = self.view.viewWithTag(tochedTag) as! UILabel
        groupTextfield.text = textFromLabel.text
        closeList()
    }
    
    func openList()
    {
        self.dropDownView.hidden = false
        
        if ((pickerHeight + 10) > (self.view.frame.height - (groupTextfield.frame.origin.y + groupTextfield.frame.height))){
            UIView.animateWithDuration(0.3,
                animations: {
                    self.dropDownView.frame = CGRect(x: self.groupTextfield.frame.origin.x, y: (self.groupTextfield.frame.origin.y - self.pickerHeight), width: self.groupTextfield.frame.width, height: self.pickerHeight)
                    
                    self.dropDownView.alpha = 1
            })
        }
        else{
            UIView.animateWithDuration(0.3,
                animations: {
                    
                    self.dropDownView.frame = CGRect(x: self.groupTextfield.frame.origin.x, y: (self.groupTextfield.frame.origin.y + self.groupTextfield.frame.height ), width: self.groupTextfield.frame.width, height: self.pickerHeight)
                    self.dropDownView.alpha = 1
            })
        }
    }
    
    func closeList()
    {
        if ((pickerHeight + 10) > (self.view.frame.height - (groupTextfield.frame.origin.y + groupTextfield.frame.height))){
            UIView.animateWithDuration(0.3,
                animations: {
                    self.dropDownView.frame = CGRect(x: self.groupTextfield.frame.origin.x, y: (self.groupTextfield.frame.origin.y - self.pickerHeight) + 10, width: self.groupTextfield.frame.width, height: self.pickerHeight)
                    self.dropDownView.alpha = 0
                },
                completion: { finished in
                    self.dropDownView.hidden = true
                }
            )
        }
        else{
            UIView.animateWithDuration(0.3,
                animations: {
                    self.dropDownView.frame = CGRect(x: self.groupTextfield.frame.origin.x, y: (self.groupTextfield.frame.origin.y + self.groupTextfield.frame.height), width: self.groupTextfield.frame.width, height: self.pickerHeight)
                    self.dropDownView.alpha = 0
                },
                completion: { finished in
                    self.dropDownView.hidden = true
                }
            )
        }
    }
    
    // =================================================================
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        textFieldTag = textField.tag
        return true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
        closePicker()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "goToMemberWelcomePage"  {
            
            // this gets a reference to the screen that we're about to transition to
            let toViewController = segue.destinationViewController as! welcomeMemberViewController
            toViewController.iphoneType = self.iphoneType!
            toViewController.transitioningDelegate = self.transitionManager
            
            // transfer user data
            toViewController.user_name = ""
            toViewController.user_email = self.user_email!
            toViewController.user_password = self.user_password!
            toViewController.user_gender = self.user_gender!
            toViewController.user_status = self.user_status!
            toViewController.user_first_name = self.user_first_name!
            toViewController.user_last_name = self.user_last_name!
            toViewController.user_email = self.user_email!
            toViewController.user_phone_number = self.user_phone_number!
            toViewController.user_group = self.user_group!
        }
    }
    
    // unwind the segue
    @IBAction func handleBackButtonClick(sender:UIButton){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}